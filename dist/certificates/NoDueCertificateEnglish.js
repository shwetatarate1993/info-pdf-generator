import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
function NoDueCertificateEnglish(data) {
    var NAME = data.NAME;
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var DISTRICT_NAME = data.DISTRICT_NAME;
    var PHOTO = data.PHOTO;
    var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    margin: 20px;\n  "], ["\n    text-align: center;\n    margin: 20px;\n  "])));
    var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n    border: 1px solid black;\n    height: 800px;\n    width: 800px;\n    margin: 0 ;\n  "], ["\n    border: 1px solid black;\n    height: 800px;\n    width: 800px;\n    margin: 0 ;\n  "])));
    var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 794px;\n    width: 794px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "], ["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 794px;\n    width: 794px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "])));
    var NoDueCertificate = styled.div(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n    margin: 0 auto;\n    padding: 30px;\n    text-align: center;\n    position: relative;\n    top: 50px;\n    border: 4px solid black;\n    width: 300px;\n    font-weight: bold;\n    border-radius: 15px;\n    font-size: 30px;\n  "], ["\n    margin: 0 auto;\n    padding: 30px;\n    text-align: center;\n    position: relative;\n    top: 50px;\n    border: 4px solid black;\n    width: 300px;\n    font-weight: bold;\n    border-radius: 15px;\n    font-size: 30px;\n  "])));
    var Logo = styled.div(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n    width: 150px;\n    height: 200px;\n    position: relative;\n    top: 100px; \n    left: 20px;\n  "], ["\n    width: 150px;\n    height: 200px;\n    position: relative;\n    top: 100px; \n    left: 20px;\n  "])));
    var Content = styled.div(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n    width: 590px;\n    height: 400px;\n    position: relative;\n    left: 175px;\n    bottom: 105px;\n  "], ["\n    width: 590px;\n    height: 400px;\n    position: relative;\n    left: 175px;\n    bottom: 105px;\n  "])));
    var ParaOne = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    font-size: 17px;\n    font-weight: bold;\n  "], ["\n    position: relative;\n    font-size: 17px;\n    font-weight: bold;\n  "])));
    var Name = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 17px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n   border-right: 1px solid white;\n   border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 17px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n   border-right: 1px solid white;\n   border-left: 1px solid white;\n  "])));
    var ParaOneTwo = styled.span(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaTwo = styled.p(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 10px;\n  font-size: 17px;\n  font-weight: bold;\n"], ["\n  position: relative;\n  bottom: 10px;\n  font-size: 17px;\n  font-weight: bold;\n"])));
    var GramPanchayat = styled.input(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 190px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 190px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
    var ParaTwoOne = styled.span(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject([""], [""])));
    var TalukaName = styled.input(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 190px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 190px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
    var ParaThree = styled.p(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 15px;\n  font-size: 17px;\n  font-weight: bold;\n"], ["\n  position: relative;\n  bottom: 15px;\n  font-size: 17px;\n  font-weight: bold;\n"])));
    var DistrictName = styled.input(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 17px;\n    font-weight: bold;\n    width: 190px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 17px;\n    font-weight: bold;\n    width: 190px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaThreeOne = styled.span(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaFour = styled.p(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 22px;\n  right: 105px;\n  font-size: 17px;\n  font-weight: bold;\n"], ["\n  position: relative;\n  bottom: 22px;\n  right: 105px;\n  font-size: 17px;\n  font-weight: bold;\n"])));
    return (React.createElement(App, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(NoDueCertificate, null, "No Due Certificate"),
                React.createElement(Logo, null,
                    React.createElement("img", { src: PHOTO, width: '150px', height: '200px' })),
                React.createElement(Content, null,
                    React.createElement(ParaOne, null,
                        "Hence, the certificate is given that Shri - \u00A0\u00A0",
                        React.createElement(Name, { value: NAME }),
                        "\u00A0\u00A0",
                        React.createElement(ParaOneTwo, null, "is")),
                    React.createElement(ParaTwo, null,
                        "a resident of village \u00A0",
                        React.createElement(GramPanchayat, { value: GRAMPANCHAYAT_NAME }),
                        "\u00A0",
                        React.createElement(ParaTwoOne, null, "Ta."),
                        "\u00A0",
                        React.createElement(TalukaName, { value: TALUKA_NAME })),
                    React.createElement(ParaThree, null,
                        "Dist. \u00A0",
                        React.createElement(DistrictName, { value: DISTRICT_NAME }),
                        "\u00A0",
                        React.createElement(ParaThreeOne, null, "is not pending due in any of the Panchayat")),
                    React.createElement(ParaFour, null, "Govt. Of India. For this, the certificate is given."))))));
}
export default NoDueCertificateEnglish;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17;
//# sourceMappingURL=NoDueCertificateEnglish.js.map
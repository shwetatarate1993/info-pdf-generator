import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
function BplCertificateGujarati(data) {
    var DISTRICT_NAME = data.DISTRICT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var NAME = data.NAME;
    var SERIAL_NO = data.SERIAL_NO;
    var dateString = data.DATE;
    var dateObj = new Date(dateString);
    var momentObj = moment(dateObj);
    var DATE = momentObj.format('YYYY-MMM-DD');
    var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    margin: 20px;\n  "], ["\n    text-align: center;\n    margin: 20px;\n  "])));
    var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n    border: 1px solid black;\n    height: 800px;\n    width: 800px;\n    margin: 0 ;\n  "], ["\n    border: 1px solid black;\n    height: 800px;\n    width: 800px;\n    margin: 0 ;\n  "])));
    var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 793px;\n    width: 792px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "], ["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 793px;\n    width: 792px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "])));
    var DistrictName = styled.p(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative;\n    right: 240px; \n  "], ["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative;\n    right: 240px; \n  "])));
    var DistrictNameInput = styled.input(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative;\n    left: 20px; \n    width: 200px;\n    border: none;\n  "], ["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative;\n    left: 20px; \n    width: 200px;\n    border: none;\n  "])));
    var TalukaName = styled.p(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative;\n    left: 235px;\n    bottom: 45px;\n  "], ["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative;\n    left: 235px;\n    bottom: 45px;\n  "])));
    var TalukaNameInput = styled.input(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative;\n    left: 20px; \n    width: 190px;\n    border: none;\n  "], ["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative;\n    left: 20px; \n    width: 190px;\n    border: none;\n  "])));
    var GramPanchayat = styled.p(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative;\n    right: 110px;\n    bottom: 50px;\n  "], ["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative;\n    right: 110px;\n    bottom: 50px;\n  "])));
    var GramPanchayatInput = styled.input(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject(["\n    font-size: 18px;\n    font-weight: bold;\n    text-align: right;\n    position: relative;\n    left: 20px; \n    width: 250px;\n    border: none;\n  "], ["\n    font-size: 18px;\n    font-weight: bold;\n    text-align: right;\n    position: relative;\n    left: 20px; \n    width: 250px;\n    border: none;\n  "])));
    var Partician = styled.p(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n    border: 3px solid black;\n    width: 99.99%;\n    position: relative;\n    bottom: 50px;\n  "], ["\n    border: 3px solid black;\n    width: 99.99%;\n    position: relative;\n    bottom: 50px;\n  "])));
    var GujaratSarkar = styled.p(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-weight: bold;\n    font-size: 20px;\n    position: relative;\n    bottom: 60px;\n  "], ["\n    text-align: center;\n    font-weight: bold;\n    font-size: 20px;\n    position: relative;\n    bottom: 60px;\n  "])));
    var BplCertificate = styled.p(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject(["\n    margin: 0 auto;\n    text-align: center;\n    font-weight: bold;\n    font-size: 25px;\n    position: relative;\n    bottom: 50px;\n    border-bottom: 1.5px solid black;\n    border-top: 1.5px solid white;\n    border-left: 1.5px solid white;\n    border-right: 1.5px solid white;\n    width: 300px; \n    word-spacing: 4px;\n    letter-spacing: 1px;\n  "], ["\n    margin: 0 auto;\n    text-align: center;\n    font-weight: bold;\n    font-size: 25px;\n    position: relative;\n    bottom: 50px;\n    border-bottom: 1.5px solid black;\n    border-top: 1.5px solid white;\n    border-left: 1.5px solid white;\n    border-right: 1.5px solid white;\n    width: 300px; \n    word-spacing: 4px;\n    letter-spacing: 1px;\n  "])));
    var Certified = styled.p(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    position: relative;\n    bottom: 50px;\n  "], ["\n    text-align: center;\n    position: relative;\n    bottom: 50px;\n  "])));
    var ParaOne = styled.p(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    left: 30px;\n    bottom: 40px;\n    font-weight: bold;\n    font-size: 16px;\n  "], ["\n    position: relative;\n    left: 30px;\n    bottom: 40px;\n    font-weight: bold;\n    font-size: 16px;\n  "])));
    var Name = styled.input(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject(["\n    font-weight: bold;\n    font-size: 16px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    text-align: center;\n  "], ["\n    font-weight: bold;\n    font-size: 16px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    text-align: center;\n  "])));
    var ParaOneOne = styled.span(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject([""], [""])));
    var GramPanchayatInputOne = styled.input(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject(["\n    font-weight: bold;\n    font-size: 16px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    text-align: center;\n  "], ["\n    font-weight: bold;\n    font-size: 16px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    text-align: center;\n  "])));
    var ParaOneTwo = styled.span(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaTwo = styled.p(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    bottom: 40px;\n    font-weight: bold;\n    font-size: 16px;\n  "], ["\n    position: relative;\n    bottom: 40px;\n    font-weight: bold;\n    font-size: 16px;\n  "])));
    var SerialNo = styled.input(templateObject_20 || (templateObject_20 = tslib_1.__makeTemplateObject(["\n    font-weight: bold;\n    font-size: 16px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    text-align: center;\n  "], ["\n    font-weight: bold;\n    font-size: 16px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    text-align: center;\n  "])));
    var ParaTwoOne = styled.span(templateObject_21 || (templateObject_21 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaThree = styled.p(templateObject_22 || (templateObject_22 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    right: 138px;\n    bottom: 40px;\n    font-weight: bold;\n    font-size: 16px;\n    word-spacing: 3px;\n  "], ["\n    position: relative;\n    right: 138px;\n    bottom: 40px;\n    font-weight: bold;\n    font-size: 16px;\n    word-spacing: 3px;\n  "])));
    var DateOne = styled.p(templateObject_23 || (templateObject_23 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    right: 230px;\n    top: 50px;\n    font-weight: bold;\n    font-size: 16px;\n  "], ["\n    position: relative;\n    right: 230px;\n    top: 50px;\n    font-weight: bold;\n    font-size: 16px;\n  "])));
    var DbDate = styled.span(templateObject_24 || (templateObject_24 = tslib_1.__makeTemplateObject(["\n    font-weight: bold;\n    font-size: 16px;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    font-weight: bold;\n    font-size: 16px;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var TalatiCumMantri = styled.p(templateObject_25 || (templateObject_25 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 80px;\n    font-weight: bold;\n    font-size: 16px;\n  "], ["\n    position: relative;\n    top: 80px;\n    font-weight: bold;\n    font-size: 16px;\n  "])));
    var GramSevak = styled.p(templateObject_26 || (templateObject_26 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    left: 300px;\n    top: 40px;\n    font-weight: bold;\n    font-size: 16px;\n  "], ["\n    position: relative;\n    left: 300px;\n    top: 40px;\n    font-weight: bold;\n    font-size: 16px;\n  "])));
    var FooterOne = styled.p(templateObject_27 || (templateObject_27 = tslib_1.__makeTemplateObject(["\n    font-size: 16px;\n    font-weight: bold;\n    position: relative;\n    right: 110px;\n    top: 38px;\n  "], ["\n    font-size: 16px;\n    font-weight: bold;\n    position: relative;\n    right: 110px;\n    top: 38px;\n  "])));
    var GramPanchayatInputTwo = styled.input(templateObject_28 || (templateObject_28 = tslib_1.__makeTemplateObject(["\n    font-size: 16px;\n    font-weight: bold;\n    text-align: right;\n    position: relative;\n    left: 20px; \n    width: 250px;\n    border: none;\n  "], ["\n    font-size: 16px;\n    font-weight: bold;\n    text-align: right;\n    position: relative;\n    left: 20px; \n    width: 250px;\n    border: none;\n  "])));
    var FooterTwo = styled.p(templateObject_29 || (templateObject_29 = tslib_1.__makeTemplateObject(["\n    font-size: 16px;\n    font-weight: bold;\n    position: relative;\n    left: 230px;\n  "], ["\n    font-size: 16px;\n    font-weight: bold;\n    position: relative;\n    left: 230px;\n  "])));
    var GramPanchayatInputThree = styled.input(templateObject_30 || (templateObject_30 = tslib_1.__makeTemplateObject(["\n    font-size: 16px;\n    font-weight: bold;\n    text-align: right;\n    position: relative;\n    left: 20px; \n    width: 150px;\n    border: none;\n  "], ["\n    font-size: 16px;\n    font-weight: bold;\n    text-align: right;\n    position: relative;\n    left: 20px; \n    width: 150px;\n    border: none;\n  "])));
    return (React.createElement(App, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(DistrictName, null,
                    "\u0A9C\u0AC0\u0AB2\u0ACD\u0AB2\u0ACB\u00A0\u00A0 -",
                    React.createElement(DistrictNameInput, { value: DISTRICT_NAME })),
                React.createElement(TalukaName, null,
                    "\u0AA4\u0ABE\u0AB2\u0AC1\u0A95\u0ACB\u00A0\u00A0 -",
                    React.createElement(TalukaNameInput, { value: TALUKA_NAME })),
                React.createElement(GramPanchayat, null,
                    React.createElement(GramPanchayatInput, { value: GRAMPANCHAYAT_NAME }),
                    "\u00A0\u00A0\u00A0\u00A0 - \u00A0\u0A97\u0ACD\u0AB0\u0ABE\u0AAE-\u0AAA\u0A82\u0A9A\u0ABE\u0AAF\u0AA4"),
                React.createElement(Partician, null),
                React.createElement(GujaratSarkar, null, "\u0A97\u0AC1\u0A9C\u0AB0\u0ABE\u0AA4 \u0AB8\u0ABE\u0AB0\u0A95\u0ABE\u0AB0"),
                React.createElement(BplCertificate, null, "\u0AAC\u0AC0\u0AAA\u0AC0\u0A8F\u0AB2 \u0A85\u0A82\u0A97\u0AC7\u0AA8\u0ACB \u0AA6\u0ABE\u0AAC\u0AB2\u0ACB"),
                React.createElement(Certified, null, "\u0A86\u0AA5\u0AC0 \u0AAA\u0ACD\u0AB0\u0AAE\u0ABE\u0AA3\u0ABF\u0AA4 \u0A95\u0AB0\u0AB5\u0ABE\u0AAE\u0ABE \u0A86\u0AB5\u0AC7 \u0A9B\u0AC7 \u0A95\u0AC7,"),
                React.createElement(ParaOne, null,
                    "\u0A85\u0AB0\u0A9C\u0AA6\u0ABE\u0AB0 \u0AB6\u0ACD\u0AB0\u0AC0 \u00A0\u00A0 ",
                    React.createElement(Name, { value: NAME }),
                    "\u00A0\u00A0",
                    React.createElement(ParaOneOne, null, "\u0AB0\u0AB9\u0AC7\u0AB5\u0ABE\u0AB8\u0AC0"),
                    "\u00A0\u00A0",
                    React.createElement(GramPanchayatInputOne, { value: GRAMPANCHAYAT_NAME }),
                    "\u00A0\u00A0",
                    React.createElement(ParaOneTwo, null, "\u0A97\u0ABE\u0AAE\u0AC7\u0AA8\u0AC0")),
                React.createElement(ParaTwo, null,
                    "\u0AAC\u0AC0\u0AAA\u0AC0\u0A8F\u0AB2 \u0AB2\u0ABE\u0AAD\u0ABE\u0AB0\u0ACD\u0AA5\u0AC0 \u0AAF\u0ABE\u0AA6\u0AC0\u0AAE\u0ABE \u0A95\u0ACD\u0AB0\u200B\u0AAE \u0AA8\u0A82\u0AAC\u0AB0 \u00A0\u00A0 ",
                    React.createElement(SerialNo, { value: SERIAL_NO }),
                    "\u00A0\u00A0",
                    React.createElement(ParaTwoOne, null, ",\u0A89\u0AAA\u0AB0 \u0AA8\u0ACB\u0A82\u0AA7\u0ABE\u0AAF\u0AC7\u0AB2 \u0A9B\u0AC7, \u0A85\u0AA8\u0AC7 \u0AA4\u0AC7\u0A93 \u0AAC\u0AC0\u0AAA\u0AC0\u0A8F\u0AB2")),
                React.createElement(ParaThree, null, "\u0AB2\u0ABE\u0AAD\u0ABE\u0AB0\u0ACD\u0AA5\u0AC0 \u0A9B\u0AC7. \u0AA4\u0AC7\u0AA8\u0AC0 \u0A96\u0ABE\u0AA4\u0ACD\u0AB0\u0AC0 \u0AAC\u0AA6\u0AB2 \u0A86 \u0AA6\u0ABE\u0A96\u0AB2\u0ACB \u0AB2\u0A96\u0AC0 \u0A86\u0AAA\u0AB5\u0ABE\u0AAE\u0ABE \u0A86\u0AB5\u0AC7 \u0A9B\u0AC7."),
                React.createElement(DateOne, null,
                    "\u0AA4\u0ABE\u0AB0\u0AC0\u0A96 : \u00A0\u00A0 ",
                    React.createElement(DbDate, null, DATE)),
                React.createElement(TalatiCumMantri, null, "\u0AA4\u0AB2\u0ABE\u0A9F\u0AC0 \u0A95\u0AAE \u0AAE\u0A82\u0AA4\u0ACD\u0AB0\u0AC0"),
                React.createElement(GramSevak, null, "\u0A97\u0ACD\u0AB0\u0ABE\u0AAE\u0AB8\u0AC7\u0AB5\u0A95"),
                React.createElement(FooterOne, null,
                    React.createElement(GramPanchayatInputTwo, { value: GRAMPANCHAYAT_NAME }),
                    "\u00A0\u00A0\u00A0\u00A0 - \u00A0\u0A97\u0ACD\u0AB0\u0ABE\u0AAE-\u0AAA\u0A82\u0A9A\u0ABE\u0AAF\u0AA4"),
                React.createElement(FooterTwo, null,
                    React.createElement(GramPanchayatInputThree, { value: GRAMPANCHAYAT_NAME }),
                    "\u00A0\u00A0\u00A0\u00A0 - \u00A0\u0A97\u0ACD\u0AB0\u0ABE.\u0AAA\u0A82.")))));
}
export default BplCertificateGujarati;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20, templateObject_21, templateObject_22, templateObject_23, templateObject_24, templateObject_25, templateObject_26, templateObject_27, templateObject_28, templateObject_29, templateObject_30;
//# sourceMappingURL=BplCertificateGujarati.js.map
import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
function BusinessCertificateGujarati(data) {
    var NAME = data.NAME;
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var DISTRICT_NAME = data.DISTRICT_NAME;
    var BUSINESS_NAME = data.BUSINESS_NAME;
    var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n    font-family: 'Times New Roman', Times, serif; \n    margin: 20px;\n  "], ["\n    font-family: 'Times New Roman', Times, serif; \n    margin: 20px;\n  "])));
    var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n    border: 1px solid black;\n    height: 800px;\n    width: 800px;\n    margin: 0 ;\n  "], ["\n    border: 1px solid black;\n    height: 800px;\n    width: 800px;\n    margin: 0 ;\n  "])));
    var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 793px;\n    width: 793px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "], ["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 793px;\n    width: 793px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "])));
    var BusinessCertificate = styled.div(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n    margin: 0 auto;\n    padding: 30px;\n    text-align: center;\n    position: relative;\n    top: 50px;\n    border: 4px solid black;\n    width: 300px;\n    font-weight: bold;\n    border-radius: 15px;\n    font-size: 30px;\n  "], ["\n    margin: 0 auto;\n    padding: 30px;\n    text-align: center;\n    position: relative;\n    top: 50px;\n    border: 4px solid black;\n    width: 300px;\n    font-weight: bold;\n    border-radius: 15px;\n    font-size: 30px;\n  "])));
    var ParaOne = styled.p(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 80px;\n    left: 70px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 2px;\n  "], ["\n    position: relative;\n    top: 80px;\n    left: 70px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 2px;\n  "])));
    var GramPanchayatName = styled.input(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 280px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 280px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var Taluka = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 80px;\n    left: 40px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 5px;\n  "], ["\n    position: relative;\n    top: 80px;\n    left: 40px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 5px;\n  "])));
    var TalukaInput = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 230px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 230px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var District = styled.span(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject([""], [""])));
    var DistrictInput = styled.input(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 230px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 230px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaTwo = styled.span(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaThree = styled.p(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 80px;\n    left: 40px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 8px;\n  "], ["\n    position: relative;\n    top: 80px;\n    left: 40px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 8px;\n  "])));
    var NameInput = styled.input(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 250px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 250px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaThreeOne = styled.span(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject([""], [""])));
    var GramPanchayatNameOne = styled.input(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 275px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 275px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaThreeTwo = styled.span(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaFour = styled.p(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 80px;\n    left: 40px;\n    font-size: 20px;\n    font-weight: bold;\n  "], ["\n    position: relative;\n    top: 80px;\n    left: 40px;\n    font-size: 20px;\n    font-weight: bold;\n  "])));
    var BusinessNameInput = styled.input(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 200px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 200px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaFourOne = styled.span(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject([""], [""])));
    return (React.createElement(App, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(BusinessCertificate, null, "\u0AB5\u0ACD\u0AAF\u0AB5\u0AB8\u0ABE\u0AAF \u0AA6\u0ABE\u0A96\u0AAD\u0ACB"),
                React.createElement(ParaOne, null,
                    "\u0A86\u0AA5\u0AC0 \u0AA6\u0ABE\u0A96\u0AB2\u0ACB \u0A86\u0AAA\u0AB5\u0ABE\u0AAE\u0ABE \u0A86\u0AB5\u0AC7 \u0A9B\u0AC7 \u0A95\u0AC7 \u0AAE\u0ACB\u0A9C\u0AC7 : \u00A0\u00A0 ",
                    React.createElement(GramPanchayatName, { value: GRAMPANCHAYAT_NAME })),
                React.createElement(Taluka, null,
                    "\u0AA4\u0ABE. \u00A0\u00A0",
                    React.createElement(TalukaInput, { value: TALUKA_NAME }),
                    "\u00A0\u00A0",
                    React.createElement(District, null, "\u0A9C\u0ABF."),
                    "\u00A0\u00A0",
                    React.createElement(DistrictInput, { value: DISTRICT_NAME }),
                    "\u00A0\u00A0",
                    React.createElement(ParaTwo, null, "\u0AA8\u0ABE \u0AB0\u0AB9\u0AC0\u0AB6")),
                React.createElement(ParaThree, null,
                    React.createElement(NameInput, { value: NAME }),
                    "\u00A0\u00A0",
                    React.createElement(ParaThreeOne, null, "\u0A9C\u0AC7\u0A93"),
                    "\u00A0\u00A0",
                    React.createElement(GramPanchayatNameOne, { value: GRAMPANCHAYAT_NAME }),
                    "\u00A0\u00A0",
                    React.createElement(ParaThreeTwo, null, "\u0A97\u0ABE\u0AAE\u0AC7")),
                React.createElement(ParaFour, null,
                    React.createElement(BusinessNameInput, { value: BUSINESS_NAME }),
                    "\u00A0\u00A0",
                    React.createElement(ParaFourOne, null, "\u0AA8\u0ACB \u0AB5\u0ACD\u0AAF\u0AB5\u0AB8\u0ABE\u0AAF \u0A95\u0AB0\u0AC7 \u0A9B\u0AC7 . \u0A9C\u0AC7 \u0AAC\u0AA6\u0AB2 \u0A86 \u0AA6\u0ABE\u0A96\u0AB2\u0ACB \u0A86\u0AAA\u0AB5\u0ABE\u0AAE\u0ABE \u0A86\u0AB5\u0AC7 \u0A9B\u0AC7 ."))))));
}
export default BusinessCertificateGujarati;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19;
//# sourceMappingURL=BusinessCertificateGujarati.js.map
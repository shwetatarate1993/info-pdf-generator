import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
function CasteCertificateGuj(data) {
    var TALUKA_NAME = data.TALUKA_NAME;
    var DISTRICT_NAME = data.DISTRICT_NAME;
    var CAST_CATEGORY = data.CAST_CATEGORY;
    var FATHER_NAME = data.FATHER_NAME;
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var NAME = data.NAME;
    var PLACE = data.PLACE;
    var RELIGION = data.RELIGION;
    var PURPOSE = data.PURPOSE;
    var PHOTO = data.PHOTO;
    var dateString = data.DATE;
    var dateObj = new Date(dateString);
    var momentObj = moment(dateObj);
    var DATE = momentObj.format('YYYY-MMM-DD');
    var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n text-align: center;\n margin:50px;\n"], ["\n text-align: center;\n margin:50px;\n"])));
    var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\nborder: 1px solid black;\nheight: 800px;\nwidth: 800px;\nmargin: 0 ;\n"], ["\nborder: 1px solid black;\nheight: 800px;\nwidth: 800px;\nmargin: 0 ;\n"])));
    var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\nborder: 6px solid black;\nmargin: 0 auto;\nheight: 793px;\nwidth: 793px;\nposition: relative;\ntop: 1.5px;\nleft: 0.3px;\n"], ["\nborder: 6px solid black;\nmargin: 0 auto;\nheight: 793px;\nwidth: 793px;\nposition: relative;\ntop: 1.5px;\nleft: 0.3px;\n"])));
    var Header = styled.p(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\nmargin: 0 auto;\npadding: 10px;\ntext-align: center;\nposition: relative;\ntop: 20px;\nborder: 4px solid black;\nwidth: 350px;\nfont-weight: bold;\nborder-radius: 15px;\nfont-size: 30px;\n"], ["\nmargin: 0 auto;\npadding: 10px;\ntext-align: center;\nposition: relative;\ntop: 20px;\nborder: 4px solid black;\nwidth: 350px;\nfont-weight: bold;\nborder-radius: 15px;\nfont-size: 30px;\n"])));
    var Photo = styled.div(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n margin: 0 auto;\n position: relative;\n top: 40px;\n width:180px;\n height:180px;\n"], ["\n margin: 0 auto;\n position: relative;\n top: 40px;\n width:180px;\n height:180px;\n"])));
    var Paras = styled.div(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\nposition:relative;\ntop:35px;\n"], ["\nposition:relative;\ntop:35px;\n"])));
    var ParaOne = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    top:40px;\n  "], ["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    top:40px;\n  "])));
    var Name = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top:white;\n    border-right:white;\n    border-left:white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n"], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top:white;\n    border-right:white;\n    border-left:white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n"])));
    var ParaOneSpan1 = styled.span(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaTwo = styled.p(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  top:30px;\n  "], ["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  top:30px;\n  "])));
    var FatherName = styled.input(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 0px solid white;\n text-align: center;\n width:250px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 0px solid white;\n text-align: center;\n width:250px;\n"])));
    var ParaTwoSpan1 = styled.span(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaThree = styled.p(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  top:30px;\n  "], ["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  top:30px;\n  "])));
    var GrampanchayatName = styled.input(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top:white;\n    border-right:white;\n    border-left:white;\n    width:200px;\n    font-family: \"Times New Roman\";\n"], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top:white;\n    border-right:white;\n    border-left:white;\n    width:200px;\n    font-family: \"Times New Roman\";\n"])));
    var TalukaName = styled.input(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:250px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:250px;\n"])));
    var ParaThreeSpan1 = styled.span(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaThreeSpan2 = styled.span(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaFour = styled.p(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject(["\nposition: relative;\nfont-size: 20px;\nfont-weight: normal;\nright:0px;\ntop:30px;\n"], ["\nposition: relative;\nfont-size: 20px;\nfont-weight: normal;\nright:0px;\ntop:30px;\n"])));
    var DistrictName = styled.input(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:250px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:250px;\n"])));
    var PlaceLabel = styled.p(templateObject_20 || (templateObject_20 = tslib_1.__makeTemplateObject(["\n font-size: 18px;\n font-weight: bold;\n position: relative;\n text-align : left;\n top:95px;\n left: 2px;\n "], ["\n font-size: 18px;\n font-weight: bold;\n position: relative;\n text-align : left;\n top:95px;\n left: 2px;\n "])));
    var PlaceInput = styled.input(templateObject_21 || (templateObject_21 = tslib_1.__makeTemplateObject(["\n font-size: 16px;\n font-weight: bold;\n position: relative;\n width: 250px;\n border-bottom: 1.5px solid black; \n border-top: 1px solid white;  \n border-right: 1px solid white;  \n border-left: 1px solid white;\n text-align: center;\n "], ["\n font-size: 16px;\n font-weight: bold;\n position: relative;\n width: 250px;\n border-bottom: 1.5px solid black; \n border-top: 1px solid white;  \n border-right: 1px solid white;  \n border-left: 1px solid white;\n text-align: center;\n "])));
    var PlaceLastInput = styled.input(templateObject_22 || (templateObject_22 = tslib_1.__makeTemplateObject(["\nfont-size: 16px;\nfont-weight: bold;\nposition: relative;\nwidth: 250px;\nborder-bottom: 1.5px solid white;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\n"], ["\nfont-size: 16px;\nfont-weight: bold;\nposition: relative;\nwidth: 250px;\nborder-bottom: 1.5px solid white;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\n"])));
    var ParaFourSpan = styled.span(templateObject_23 || (templateObject_23 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaFive = styled.p(templateObject_24 || (templateObject_24 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  right:0px;\n  top:30px;\n  "], ["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  right:0px;\n  top:30px;\n  "])));
    var Religion = styled.input(templateObject_25 || (templateObject_25 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:200px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:200px;\n"])));
    var ParaFiveSpan1 = styled.span(templateObject_26 || (templateObject_26 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaFiveSpan2 = styled.span(templateObject_27 || (templateObject_27 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaFiveSpan3 = styled.span(templateObject_28 || (templateObject_28 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaSix = styled.p(templateObject_29 || (templateObject_29 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  right:0px;\n  top:30px;\n  "], ["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  right:0px;\n  top:30px;\n  "])));
    var CasteCatagary = styled.input(templateObject_30 || (templateObject_30 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:200px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:200px;\n"])));
    var ParaSixSpan = styled.span(templateObject_31 || (templateObject_31 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaSeven = styled.p(templateObject_32 || (templateObject_32 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  right:0px;\n  top:30px;\n  left:60px;\n  text-align:left;\n  "], ["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  right:0px;\n  top:30px;\n  left:60px;\n  text-align:left;\n  "])));
    var Purpose = styled.input(templateObject_33 || (templateObject_33 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:230px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:230px;\n"])));
    var ParaSevenSpan = styled.span(templateObject_34 || (templateObject_34 = tslib_1.__makeTemplateObject([""], [""])));
    var DateLabel = styled.p(templateObject_35 || (templateObject_35 = tslib_1.__makeTemplateObject(["\n font-size: 18px;\n font-weight: bold;\n position: relative;\n text-align : left;\n top:95px;\n left: 2px;\n "], ["\n font-size: 18px;\n font-weight: bold;\n position: relative;\n text-align : left;\n top:95px;\n left: 2px;\n "])));
    var DateInput = styled.input(templateObject_36 || (templateObject_36 = tslib_1.__makeTemplateObject(["\n font-size: 16px;\n font-weight: bold;\n position: relative;\n width: 100px;\n border: none;\n "], ["\n font-size: 16px;\n font-weight: bold;\n position: relative;\n width: 100px;\n border: none;\n "])));
    var TalatiCumMantri = styled.label(templateObject_37 || (templateObject_37 = tslib_1.__makeTemplateObject(["\n position: relative;\n top: 55px;\n font-size: 18px;\n font-weight: bold;\n left:220px;\n text-align :center;\n "], ["\n position: relative;\n top: 55px;\n font-size: 18px;\n font-weight: bold;\n left:220px;\n text-align :center;\n "])));
    var Footers = styled.div(templateObject_38 || (templateObject_38 = tslib_1.__makeTemplateObject(["\n  position:relative;\n  left:50px;\n  "], ["\n  position:relative;\n  left:50px;\n  "])));
    return (React.createElement(App, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(Header, null, "\u0A9C\u0ABE\u0AA4\u0ABF\u0AA8\u0AC1\u0A82 \u0AAA\u0ACD\u0AB0\u0AAE\u0ABE\u0AA3\u0AAA\u0AA4\u0ACD\u0AB0"),
                React.createElement(Photo, null,
                    React.createElement("img", { src: PHOTO, width: '180px', height: '180px' })),
                React.createElement(Paras, null,
                    React.createElement(ParaOne, null,
                        React.createElement(ParaOneSpan1, null, "\u0A86\u0AA5\u0AC0 \u0AA6\u0ABE\u0A96\u0AB2\u0ACB \u0A86\u0AAA\u0AB5\u0ABE\u0AAE\u0ABE \u0A86\u0AB5\u0AC7 \u0A9B\u0AC7 \u0A95\u0AC7, \u0AB6\u0ACD\u0AB0\u0AC0"),
                        React.createElement(Name, { value: NAME }),
                        React.createElement(ParaOneSpan1, null, "\u0AA8\u0ABE \u0AAA\u0ABF\u0AA4\u0ABE/  ")),
                    React.createElement(ParaTwo, null,
                        "\u0AB5\u0ABE\u0AB2\u0AC0 \u0AB6\u0ACD\u0AB0\u0AC0",
                        React.createElement(FatherName, { value: FATHER_NAME }),
                        React.createElement(ParaTwoSpan1, null, "  \u0AB0\u0AB9\u0AC7. \u0AAE\u0ACB\u0A9C\u0AC7  "),
                        React.createElement(GrampanchayatName, { value: GRAMPANCHAYAT_NAME })),
                    React.createElement(ParaThree, null,
                        "\u0AA4\u0ABE.",
                        React.createElement(TalukaName, { value: TALUKA_NAME }),
                        React.createElement(ParaThreeSpan2, null, "\u00A0 \u0A9C\u0ABF. "),
                        React.createElement(DistrictName, { value: DISTRICT_NAME }),
                        "\u0AA8\u0ABE"),
                    React.createElement(ParaFour, null,
                        React.createElement(PlaceInput, { value: PLACE }),
                        "   \u0AAB\u0AB0\u0ABF\u0AAF\u0ABE   /  \u0AB8\u0ACB\u0AB8\u0ABE\u0AAF\u0A9F\u0AC0\u0AA8\u0ABE   \u0AB0\u0AB9\u0AC0\u0AB6  \u0A9B\u0AC7 .  \u0AA4\u0AC7\u0A93"),
                    React.createElement(ParaFive, null,
                        React.createElement(Religion, { value: RELIGION }),
                        React.createElement(ParaFiveSpan3, null, "\u0A9C\u0ACD\u0A9E\u0ABE\u0AA4\u0ABF\u0AA8\u0ABE \u0A9B\u0AC7. \u0AA4\u0AC7\u0A93\u0AA8\u0ACB \u0AB8\u0AAE\u0ABE\u0AB5\u0AC7\u0AB6 "),
                        " ",
                        React.createElement(CasteCatagary, { value: CAST_CATEGORY })),
                    React.createElement(ParaSix, null,
                        "\u0A9C\u0ABE\u0AA4\u0ABF\u0AAE\u0ABE\u0A82  \u0AA5\u0AAF\u0AC7\u0AB2 \u0A9B\u0AC7 . \u0AB8\u0AA6\u0AB0\u0AB9\u0AC1  \u0AA6\u0ABE\u0A96\u0AB2\u0ACB ",
                        React.createElement(Purpose, { value: PURPOSE }),
                        React.createElement(ParaFiveSpan3, null, "\u0AB9\u0AC7\u0AA4\u0AC1 \u0AAE\u0ABE\u0A9F\u0AC7 \u0A86\u0AAA\u0AB5\u0ABE\u0AAE\u0ABE\u0A82")),
                    React.createElement(ParaSeven, null, "\u0A86\u0AB5\u0AC7 \u0A9B\u0AC7.")),
                React.createElement(Footers, null,
                    React.createElement(PlaceLabel, null,
                        "\u0AB8\u0ACD\u0AA5\u0AB3: \u00A0\u00A0",
                        React.createElement(PlaceLastInput, { value: PLACE })),
                    React.createElement(DateLabel, null,
                        "\u0AA4\u0ABE\u0AB0\u0AC0\u0A96: \u00A0\u00A0",
                        React.createElement(DateInput, { value: DATE })),
                    React.createElement(TalatiCumMantri, null, "\u0AA4\u0AB2\u0ABE\u0A9F\u0AC0 \u0A95\u0AAE \u0AAE\u0A82\u0AA4\u0ACD\u0AB0\u0AC0"))))));
}
export default CasteCertificateGuj;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20, templateObject_21, templateObject_22, templateObject_23, templateObject_24, templateObject_25, templateObject_26, templateObject_27, templateObject_28, templateObject_29, templateObject_30, templateObject_31, templateObject_32, templateObject_33, templateObject_34, templateObject_35, templateObject_36, templateObject_37, templateObject_38;
//# sourceMappingURL=CasteCertificateGuj.js.map
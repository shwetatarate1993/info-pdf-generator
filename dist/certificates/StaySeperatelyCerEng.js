import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
function StaySeperatelyCerEng(data) {
    var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    margin: 20px;\n  "], ["\n    text-align: center;\n    margin: 20px;\n  "])));
    var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n    border: 1px solid black;\n    height: 800px;\n    width: 800px;\n    margin: 0 ;\n  "], ["\n    border: 1px solid black;\n    height: 800px;\n    width: 800px;\n    margin: 0 ;\n  "])));
    var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 794px;\n    width: 794px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "], ["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 794px;\n    width: 794px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "])));
    var StaySeparatelyCer = styled.div(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n    margin: 0 auto;\n    padding: 30px;\n    text-align: center;\n    position: relative;\n    top: 50px;\n    border: 4px solid black;\n    width: 400px;\n    font-weight: bold;\n    border-radius: 15px;\n    font-size: 30px;\n  "], ["\n    margin: 0 auto;\n    padding: 30px;\n    text-align: center;\n    position: relative;\n    top: 50px;\n    border: 4px solid black;\n    width: 400px;\n    font-weight: bold;\n    border-radius: 15px;\n    font-size: 30px;\n  "])));
    var ParaOne = styled.p(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 80px;\n    left: 10px;\n    font-size: 20px;\n    font-weight: bold;\n  "], ["\n    position: relative;\n    top: 80px;\n    left: 10px;\n    font-size: 20px;\n    font-weight: bold;\n  "])));
    var Name = styled.input(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaOneOne = styled.span(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaTwo = styled.p(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top:70px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 3px;\n  "], ["\n    position: relative;\n    top:70px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 3px;\n  "])));
    var GramPanchayat = styled.input(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 270px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 200px;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 270px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 200px;\n  "])));
    var Taluka = styled.span(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject([""], [""])));
    var TalukaInput = styled.input(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 275px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 200px;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 275px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 200px;\n  "])));
    var ParaTwoOne = styled.span(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaThree = styled.p(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 60px;\n    right: 10px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 2px;\n  "], ["\n    position: relative;\n    top: 60px;\n    right: 10px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 2px;\n  "])));
    var PropertyNo = styled.input(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaThreeOne = styled.span(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject([""], [""])));
    var FamilyMember1 = styled.p(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject(["\n    text-align: left;\n    position: relative;\n    top: 55px;\n    left: 10px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 2px;\n  "], ["\n    text-align: left;\n    position: relative;\n    top: 55px;\n    left: 10px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 2px;\n  "])));
    var FmailyMemberInput1 = styled.input(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var FamilyMember2 = styled.p(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject(["\n    text-align: left;\n    position: relative;\n    top: 50px;\n    left: 10px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 2px;\n  "], ["\n    text-align: left;\n    position: relative;\n    top: 50px;\n    left: 10px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 2px;\n  "])));
    var FmailyMemberInput2 = styled.input(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var FamilyMember3 = styled.p(templateObject_20 || (templateObject_20 = tslib_1.__makeTemplateObject(["\n    text-align: left;\n    position: relative;\n    top: 50px;\n    left: 10px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 2px;\n  "], ["\n    text-align: left;\n    position: relative;\n    top: 50px;\n    left: 10px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 2px;\n  "])));
    var FmailyMemberInput3 = styled.input(templateObject_21 || (templateObject_21 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var FamilyMember4 = styled.p(templateObject_22 || (templateObject_22 = tslib_1.__makeTemplateObject(["\n    text-align: left;\n    position: relative;\n    top: 50px;\n    left: 10px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 2px;\n  "], ["\n    text-align: left;\n    position: relative;\n    top: 50px;\n    left: 10px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 2px;\n  "])));
    var FmailyMemberInput4 = styled.input(templateObject_23 || (templateObject_23 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaFour = styled.p(templateObject_24 || (templateObject_24 = tslib_1.__makeTemplateObject(["\n    text-align: left;\n    position: relative;\n    top: 60px;\n    left: 10px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 2px;\n  "], ["\n    text-align: left;\n    position: relative;\n    top: 60px;\n    left: 10px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 2px;\n  "])));
    var NameOne = styled.input(templateObject_25 || (templateObject_25 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaFourOne = styled.span(templateObject_26 || (templateObject_26 = tslib_1.__makeTemplateObject([""], [""])));
    var NAME = data.NAME;
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var PROPERTY_NO = data.PROPERTY_NO;
    var FAMILY_MEMBER_NAME = data.FAMILY_MEMBER_NAME;
    var FAMILY_MEMBER_NAME_TWO = data.FAMILY_MEMBER_NAME_TWO;
    var FAMILY_MEMBER_NAME_THREE = data.FAMILY_MEMBER_NAME_THREE;
    var FAMILY_MEMBER_NAME_FOUR = data.FAMILY_MEMBER_NAME_FOUR;
    return (React.createElement(App, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(StaySeparatelyCer, null, "Stay Separately Certificate"),
                React.createElement(ParaOne, null,
                    "This is the certificate given to :",
                    React.createElement(Name, { value: NAME }),
                    React.createElement(ParaOneOne, null, "resident of village")),
                React.createElement(ParaTwo, null,
                    React.createElement(GramPanchayat, { value: GRAMPANCHAYAT_NAME }),
                    "\u00A0\u00A0",
                    React.createElement(Taluka, null, "Ta."),
                    "\u00A0\u00A0",
                    React.createElement(TalukaInput, { value: TALUKA_NAME }),
                    "\u00A0\u00A0",
                    React.createElement(ParaTwoOne, null, "having Property Number :")),
                React.createElement(ParaThree, null,
                    "\u00A0\u00A0",
                    React.createElement(PropertyNo, { value: PROPERTY_NO }),
                    "\u00A0\u00A0",
                    React.createElement(ParaThreeOne, null, ". They stay separate with their following family")),
                React.createElement(FamilyMember1, null,
                    "1. \u00A0\u00A0",
                    React.createElement(FmailyMemberInput1, { value: FAMILY_MEMBER_NAME })),
                React.createElement(FamilyMember2, null,
                    "2. \u00A0\u00A0",
                    React.createElement(FmailyMemberInput2, { value: FAMILY_MEMBER_NAME_TWO })),
                React.createElement(FamilyMember3, null,
                    "3. \u00A0\u00A0",
                    React.createElement(FmailyMemberInput3, { value: FAMILY_MEMBER_NAME_THREE })),
                React.createElement(FamilyMember4, null,
                    "4. \u00A0\u00A0",
                    React.createElement(FmailyMemberInput4, { value: FAMILY_MEMBER_NAME_FOUR })),
                React.createElement(ParaFour, null,
                    "With the above mentioned family,",
                    React.createElement(NameOne, { value: NAME }),
                    React.createElement(ParaFourOne, null, "remain separate from their family. For this, stay separately certification is given."))))));
}
export default StaySeperatelyCerEng;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20, templateObject_21, templateObject_22, templateObject_23, templateObject_24, templateObject_25, templateObject_26;
//# sourceMappingURL=StaySeperatelyCerEng.js.map
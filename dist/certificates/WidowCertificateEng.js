import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
function WidowCertificateEng(data) {
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var DISTRICT_NAME = data.DISTRICT_NAME;
    var NAME = data.NAME;
    var HUSBAND_NAME = data.HUSBAND_NAME;
    var dateString = data.DATE;
    var dateObj = new Date(dateString);
    var momentObj = moment(dateObj);
    var DATE = momentObj.format('YYYY-MMM-DD');
    var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n text-align: left;\n margin: 30px;\n"], ["\n text-align: left;\n margin: 30px;\n"])));
    var Border = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\nborder: 6px solid black;\nmargin: 0 ;\nheight: 500px;\nwidth: 756px;\nposition: relative;\ntop: 1.5px;\n"], ["\nborder: 6px solid black;\nmargin: 0 ;\nheight: 500px;\nwidth: 756px;\nposition: relative;\ntop: 1.5px;\n"])));
    var Header = styled.p(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\nmargin: 0 auto;\npadding-right: 20px;\npadding-left: 20px;\npadding-top: 7px;\npadding-bottom: 20px;\ntext-align: center;\nposition: relative;\ntop: 20px;\nborder: 4px solid black;\nwidth: 350px;\nfont-weight: normal;\nborder-radius: 15px;\nfont-size: 25px;\n"], ["\nmargin: 0 auto;\npadding-right: 20px;\npadding-left: 20px;\npadding-top: 7px;\npadding-bottom: 20px;\ntext-align: center;\nposition: relative;\ntop: 20px;\nborder: 4px solid black;\nwidth: 350px;\nfont-weight: normal;\nborder-radius: 15px;\nfont-size: 25px;\n"])));
    var ParaOne = styled.p(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n   position: relative;\n   top: 60px;\n   left: 30px;\n   right:20px;\n   font-size: 20px;\n   font-weight: normal;\n   margin-right: 30px;\n "], ["\n   position: relative;\n   top: 60px;\n   left: 30px;\n   right:20px;\n   font-size: 20px;\n   font-weight: normal;\n   margin-right: 30px;\n "])));
    var ParaSpan1 = styled.span(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject([""], [""])));
    var Name = styled.input(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:200px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:200px;\n"])));
    var ParaTwo = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  top: 55px;\n  left: 30px;\n  font-size: 20px;\n  font-weight: normal;\n"], ["\n  position: relative;\n  top: 55px;\n  left: 30px;\n  font-size: 20px;\n  font-weight: normal;\n"])));
    var Para2Span1 = styled.span(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject([""], [""])));
    var Para2Span2 = styled.span(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject([""], [""])));
    var HusbandName = styled.input(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:300px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:300px;\n"])));
    var DateOne = styled.input(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\nfont-weight: bold;\nfont-size: 16px;\nborder-bottom: 1.5px solid black;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\ntext-align: center;\nwidth:200px;\n "], ["\nfont-weight: bold;\nfont-size: 16px;\nborder-bottom: 1.5px solid black;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\ntext-align: center;\nwidth:200px;\n "])));
    var ParaThree = styled.p(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject(["\n   position: relative;\n   font-size: 20px;\n   font-weight: bold;\n   top:48px;\n   left: 30px;\n  "], ["\n   position: relative;\n   font-size: 20px;\n   font-weight: bold;\n   top:48px;\n   left: 30px;\n  "])));
    var Para3Span1 = styled.span(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject([""], [""])));
    var Para3Span2 = styled.span(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject([""], [""])));
    var GrampanchayatName = styled.input(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 18px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:300px;\n"], ["\n font-weight: bold;\n font-size: 18px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:300px;\n"])));
    var TalukaName = styled.input(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:300px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:300px;\n"])));
    var ParaFour = styled.p(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    top:40px;\n    left:30px;\n"], ["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    top:40px;\n    left:30px;\n"])));
    var Para4Span1 = styled.span(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject([""], [""])));
    var DistrictName = styled.input(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 18px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:200px;\n"], ["\n font-weight: bold;\n font-size: 18px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:200px;\n"])));
    var ParaFive = styled.p(templateObject_20 || (templateObject_20 = tslib_1.__makeTemplateObject(["\n   position: relative;\n   font-size: 20px;\n   font-weight: normal;\n  left:30px;\n  top:30px;\n"], ["\n   position: relative;\n   font-size: 20px;\n   font-weight: normal;\n  left:30px;\n  top:30px;\n"])));
    var SarpanchShri = styled.label(templateObject_21 || (templateObject_21 = tslib_1.__makeTemplateObject(["\n font-size: 20px;\n font-weight: normal;\n position: relative;\n text-align : left;\n top:100px;\n left: 30px;\n "], ["\n font-size: 20px;\n font-weight: normal;\n position: relative;\n text-align : left;\n top:100px;\n left: 30px;\n "])));
    var TalatiCumMantri = styled.label(templateObject_22 || (templateObject_22 = tslib_1.__makeTemplateObject(["\n position: relative;\n top: 100px;\n font-size: 20px;\n font-weight: normal;\n left:420px;\n "], ["\n position: relative;\n top: 100px;\n font-size: 20px;\n font-weight: normal;\n left:420px;\n "])));
    return (React.createElement(App, null,
        React.createElement(Border, { id: 'download-div' },
            React.createElement(Header, null, "Widow Certificate "),
            React.createElement(ParaOne, null,
                "This is the Certificate that is given to: \u00A0\u00A0",
                React.createElement(Name, { value: NAME }),
                React.createElement(ParaSpan1, null, ", whose husband  ")),
            React.createElement(ParaTwo, null,
                React.createElement(HusbandName, { value: HUSBAND_NAME }),
                React.createElement(Para2Span1, null, "died \u00A0\u00A0on "),
                React.createElement(DateOne, { value: DATE }),
                React.createElement(Para2Span2, null, "resident \u00A0\u00A0of ")),
            React.createElement(ParaThree, null,
                React.createElement(GrampanchayatName, { value: GRAMPANCHAYAT_NAME }),
                React.createElement(Para3Span1, null, "Ta. \u00A0\u00A0"),
                React.createElement(TalukaName, { value: TALUKA_NAME }),
                React.createElement(Para3Span2, null, "Dist.")),
            React.createElement(ParaFour, null,
                React.createElement(DistrictName, { value: DISTRICT_NAME }),
                React.createElement(Para4Span1, null, ",So she is widowed since then. This is done by ensuring ")),
            React.createElement(ParaFive, null, "that this is done."),
            React.createElement(SarpanchShri, null, "Sarpanch Shri"),
            React.createElement(TalatiCumMantri, null, "Talati Cum Mantri"))));
}
export default WidowCertificateEng;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20, templateObject_21, templateObject_22;
//# sourceMappingURL=WidowCertificateEng.js.map
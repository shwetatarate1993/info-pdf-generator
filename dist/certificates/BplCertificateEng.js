import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
var Default = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  margin: 20px;\n  font-family: \"ChelthmlITC BK BT\";\n"], ["\n  text-align: center;\n  margin: 20px;\n  font-family: \"ChelthmlITC BK BT\";\n"])));
var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n  border: 1px solid black;\n  height: 600px;\n  width: 798px;\n  margin: 0 ;\n"], ["\n  border: 1px solid black;\n  height: 600px;\n  width: 798px;\n  margin: 0 ;\n"])));
var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\n  border: 6px solid black;\n  margin: 0 auto;\n  height: 595px;\n  width: 793px;\n  position: relative;\n  top: 1.5px;\n  left: 0.3px;\n"], ["\n  border: 6px solid black;\n  margin: 0 auto;\n  height: 595px;\n  width: 793px;\n  position: relative;\n  top: 1.5px;\n  left: 0.3px;\n"])));
var BorderThree = styled.div(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n  border: 1px solid black;\n  margin: 0 auto;\n  height: 579px;\n  width: 777px;\n  position: relative;\n  top: 2px;\n"], ["\n  border: 1px solid black;\n  margin: 0 auto;\n  height: 579px;\n  width: 777px;\n  position: relative;\n  top: 2px;\n"])));
var DistrictLabel = styled.p(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n  font-size: 20px;\n  font-weight: normal;\n  position: relative;\n  right: 200px;\n  top: 5px; \n"], ["\n  font-size: 20px;\n  font-weight: normal;\n  position: relative;\n  right: 200px;\n  top: 5px; \n"])));
var DistrictInput = styled.input(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n  font-size: 18px;\n  font-weight: bold;\n  position: relative; \n  text-align: center;\n  width: 200px;\n  border: none;\n  font-family: \"Times New Roman\";\n"], ["\n  font-size: 18px;\n  font-weight: bold;\n  position: relative; \n  text-align: center;\n  width: 200px;\n  border: none;\n  font-family: \"Times New Roman\";\n"])));
var TalukaLabel = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n  font-size: 20px;\n  font-weight: normal;\n  position: relative;\n  left: 250px;\n  bottom: 42px;\n"], ["\n  font-size: 20px;\n  font-weight: normal;\n  position: relative;\n  left: 250px;\n  bottom: 42px;\n"])));
var TalukaInput = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n  font-size: 18px;\n  font-weight: bold;\n  text-align: center;\n  position: relative;\n  width: 190px;\n  border: none;\n  font-family: \"Times New Roman\";\n"], ["\n  font-size: 18px;\n  font-weight: bold;\n  text-align: center;\n  position: relative;\n  width: 190px;\n  border: none;\n  font-family: \"Times New Roman\";\n"])));
var GrampanchayatLabel = styled.p(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject(["\n  font-size: 20px;\n  font-weight: normal;\n  position: relative;\n  bottom: 55px;\n  right: 90px;\n"], ["\n  font-size: 20px;\n  font-weight: normal;\n  position: relative;\n  bottom: 55px;\n  right: 90px;\n"])));
var GrampanchayatName = styled.input(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n  font-size: 18px;\n  font-weight: bold;\n  text-align: right;\n  position: relative;\n  width: 270px;\n  border: none;\n  font-family: \"Times New Roman\";\n"], ["\n  font-size: 18px;\n  font-weight: bold;\n  text-align: right;\n  position: relative;\n  width: 270px;\n  border: none;\n  font-family: \"Times New Roman\";\n"])));
var Line = styled.p(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n  border: 1px solid black;\n  width: 75%;\n  position: relative;\n  bottom: 70px; \n  left:70px;\n"], ["\n  border: 1px solid black;\n  width: 75%;\n  position: relative;\n  bottom: 70px; \n  left:70px;\n"])));
var GovernmentName = styled.label(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject(["\n  margin: 0 auto;\n  padding: 30px;\n  text-align: center;\n  position: relative;\n  width: 300px;\n  font-weight: bold;\n  font-size: 20px;\n  bottom: 94px;\n"], ["\n  margin: 0 auto;\n  padding: 30px;\n  text-align: center;\n  position: relative;\n  width: 300px;\n  font-weight: bold;\n  font-size: 20px;\n  bottom: 94px;\n"])));
var BplCertificate = styled.div(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n  margin: 0 auto;\n  padding: 10px;\n  text-align: center;\n  position: relative;\n  font-size: 20px;\n  border: 3px solid black;\n  width: 250px;\n  font-weight: bold;\n  border-radius: 13px;\n  bottom: 120px;\n"], ["\n  margin: 0 auto;\n  padding: 10px;\n  text-align: center;\n  position: relative;\n  font-size: 20px;\n  border: 3px solid black;\n  width: 250px;\n  font-weight: bold;\n  border-radius: 13px;\n  bottom: 120px;\n"])));
var InfoLine = styled.p(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    bottom: 120px;\n    left: 3px;\n    font-size: 15px;\n    font-weight: normal;\n  "], ["\n    position: relative;\n    bottom: 120px;\n    left: 3px;\n    font-size: 15px;\n    font-weight: normal;\n  "])));
var ParaOne = styled.p(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    bottom: 90px;\n    font-size: 20px;\n    font-weight: normal;\n  "], ["\n    position: relative;\n    bottom: 90px;\n    font-size: 20px;\n    font-weight: normal;\n  "])));
var Name = styled.input(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 18px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n  font-family: \"Times New Roman\";\n"], ["\n  text-align: center;\n  font-size: 18px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n  font-family: \"Times New Roman\";\n"])));
var ParaOneSpan = styled.span(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject([""], [""])));
var GrampanchayatNamePara = styled.input(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 18px;\n  font-weight: bold;\n  width: 200px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n  font-family: \"Times New Roman\";\n"], ["\n  text-align: center;\n  font-size: 18px;\n  font-weight: bold;\n  width: 200px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n  font-family: \"Times New Roman\";\n"])));
var ParaTwo = styled.p(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 105px;\n  right:25px;\n  font-size: 20px;\n  font-weight: normal;\n"], ["\n  position: relative;\n  bottom: 105px;\n  right:25px;\n  font-size: 20px;\n  font-weight: normal;\n"])));
var SerialNo = styled.input(templateObject_20 || (templateObject_20 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 18px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n  font-family: \"Times New Roman\";\n"], ["\n  text-align: center;\n  font-size: 18px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n  font-family: \"Times New Roman\";\n"])));
var ParaTwoSpan = styled.span(templateObject_21 || (templateObject_21 = tslib_1.__makeTemplateObject([""], [""])));
var ParaThree = styled.p(templateObject_22 || (templateObject_22 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 123px;\n  font-size: 20px;\n  font-weight: normal;\n  right:50px;\n"], ["\n  position: relative;\n  bottom: 123px;\n  font-size: 20px;\n  font-weight: normal;\n  right:50px;\n"])));
var DateLabel = styled.p(templateObject_23 || (templateObject_23 = tslib_1.__makeTemplateObject(["\n  font-size: 20px;\n  font-weight: normal;\n  position: relative;\n  text-align : left; \n  bottom:120px;\n  left:20px;\n"], ["\n  font-size: 20px;\n  font-weight: normal;\n  position: relative;\n  text-align : left; \n  bottom:120px;\n  left:20px;\n"])));
var DateInput = styled.input(templateObject_24 || (templateObject_24 = tslib_1.__makeTemplateObject(["\n  font-size: 18px;\n  font-weight: bold;\n  position: relative;\n  width: 120px;\n  font-family: \"Times New Roman\";\n  border: none;\n"], ["\n  font-size: 18px;\n  font-weight: bold;\n  position: relative;\n  width: 120px;\n  font-family: \"Times New Roman\";\n  border: none;\n"])));
var TalatiCumMantri = styled.label(templateObject_25 || (templateObject_25 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 130px;\n  font-size: 20px;\n  font-weight: normal;\n  left: 250px;\n"], ["\n  position: relative;\n  bottom: 130px;\n  font-size: 20px;\n  font-weight: normal;\n  left: 250px;\n"])));
var GramSevek = styled.label(templateObject_26 || (templateObject_26 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 130px;\n  font-size: 20px;\n  font-weight: normal;\n  left:450px;\n"], ["\n  position: relative;\n  bottom: 130px;\n  font-size: 20px;\n  font-weight: normal;\n  left:450px;\n"])));
var GrampanchayatLabel2 = styled.label(templateObject_27 || (templateObject_27 = tslib_1.__makeTemplateObject(["\n  font-size: 18px;\n  position: relative;\n  font-weight: normal;\n  bottom:80px;\n  right: 300px;\n"], ["\n  font-size: 18px;\n  position: relative;\n  font-weight: normal;\n  bottom:80px;\n  right: 300px;\n"])));
var GrampanchayatName2 = styled.input(templateObject_28 || (templateObject_28 = tslib_1.__makeTemplateObject(["\n  font-size: 18px;\n  font-weight: bold;\n  text-align: right;\n  position: relative;\n  width: 270px;\n  border: none;\n  font-family: \"Times New Roman\";\n"], ["\n  font-size: 18px;\n  font-weight: bold;\n  text-align: right;\n  position: relative;\n  width: 270px;\n  border: none;\n  font-family: \"Times New Roman\";\n"])));
var GrampanchayatLabel3 = styled.p(templateObject_29 || (templateObject_29 = tslib_1.__makeTemplateObject(["\n  font-size: 18px;\n  font-weight: normal;\n  position: relative;\n  bottom: 120px;\n  left:220px;\n"], ["\n  font-size: 18px;\n  font-weight: normal;\n  position: relative;\n  bottom: 120px;\n  left:220px;\n"])));
var GrampanchayatName3 = styled.input(templateObject_30 || (templateObject_30 = tslib_1.__makeTemplateObject(["\n  font-size: 18px;\n  font-weight: bold;\n  position: relative;\n  text-align: right;\n  width: 270px;\n  border: none;\n  font-family: \"Times New Roman\";\n"], ["\n  font-size: 18px;\n  font-weight: bold;\n  position: relative;\n  text-align: right;\n  width: 270px;\n  border: none;\n  font-family: \"Times New Roman\";\n"])));
function BplCertificateEng(data) {
    var DISTRICT_NAME = data.DISTRICT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var NAME = data.NAME;
    var SERIAL_NO = data.SERIAL_NO;
    var dateString = data.DATE;
    var dateObj = new Date(dateString);
    var momentObj = moment(dateObj);
    var DATE = momentObj.format('YYYY-MMM-DD');
    return (React.createElement(Default, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(BorderThree, null,
                    React.createElement(DistrictLabel, null,
                        "District-",
                        React.createElement(DistrictInput, { value: DISTRICT_NAME })),
                    React.createElement(TalukaLabel, null,
                        "Taluka-",
                        React.createElement(TalukaInput, { value: TALUKA_NAME })),
                    React.createElement(GrampanchayatLabel, null,
                        React.createElement(GrampanchayatName, { value: GRAMPANCHAYAT_NAME }),
                        " - Gram Panchayat"),
                    React.createElement(Line, null),
                    React.createElement(GovernmentName, null, "Government of Gujarat"),
                    React.createElement(BplCertificate, null, "BPL Certificate"),
                    React.createElement(InfoLine, null, "It is therefore certified that,"),
                    React.createElement(ParaOne, null,
                        "The applicant Mr. \u00A0\u00A0",
                        React.createElement(Name, { value: NAME }),
                        "\u00A0\u00A0",
                        React.createElement(ParaOneSpan, null, "resident of "),
                        "\u00A0\u00A0",
                        React.createElement(GrampanchayatNamePara, { value: GRAMPANCHAYAT_NAME }),
                        "\u00A0\u00A0 village"),
                    React.createElement(ParaTwo, null,
                        "is  registered  in  the  BPL  beneficiaries  list  on the number",
                        React.createElement(SerialNo, { value: SERIAL_NO }),
                        React.createElement(ParaTwoSpan, null, ", and")),
                    React.createElement(ParaThree, null, "they \u00A0 are\u00A0 BPL\u00A0 beneficiaries.\u00A0 This \u00A0certificate\u00A0 is\u00A0 written \u00A0for \u00A0its \u00A0confirmation."),
                    React.createElement(DateLabel, null,
                        "Date: \u00A0",
                        React.createElement(DateInput, { value: DATE })),
                    React.createElement(TalatiCumMantri, null, "Talati Cum Mantri"),
                    React.createElement(GramSevek, null, "Gram Sevek"),
                    React.createElement(GrampanchayatLabel2, null,
                        React.createElement(GrampanchayatName2, { value: GRAMPANCHAYAT_NAME }),
                        " -Gram Panchayat"),
                    React.createElement(GrampanchayatLabel3, null,
                        React.createElement(GrampanchayatName3, { value: GRAMPANCHAYAT_NAME }),
                        "-G.P."))))));
}
export default BplCertificateEng;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20, templateObject_21, templateObject_22, templateObject_23, templateObject_24, templateObject_25, templateObject_26, templateObject_27, templateObject_28, templateObject_29, templateObject_30;
//# sourceMappingURL=BplCertificateEng.js.map
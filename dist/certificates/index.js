import * as tslib_1 from "tslib";
import React from 'react';
import BirthCertificate from './BirthCertificate';
import DeathCertificate from './DeathCertificate';
import BplCertificateEng from './BplCertificateEng';
import BplCertificateGujarati from './BplCertificateGujarati';
import BusinessCertificateEnglish from './BusinessCertificateEnglish';
import BusinessCertificateGujarati from './BusinessCertificateGujarati';
import NoDueCertificateEnglish from './NoDueCertificateEnglish';
import NoDueCertificateGujarati from './NoDueCertificateGujarati';
import ResidencyCertificateEnglish from './ResidencyCertificateEnglish';
import ResidencyCertificateGujarati from './ResidencyCertificateGujarati';
import FarmerCertificateEnglish from './FarmerCertificateEnglish';
import FarmerCertificateGujarati from './FarmerCertificateGujarati';
import StaySeperatelyCerEng from './StaySeperatelyCerEng';
import StaySeperatelyCerGuj from './StaySeperatelyCerGuj';
import IncomeCertificateEnglish from './IncomeCertificateEnglish';
import IncomeCertificateGujarati from './IncomeCertificateGujarati';
import MarriageCertificateEnglish from './MarriageCertificateEnglish';
import MarriageCertificateGujarati from './MarriageCertificateGujarati';
import PropertyInsideCertificateEng from './PropertyInsideCertificateEng';
import PropertyInsideCertificateGuj from './PropertyInsideCertificateGuj';
import NotRemarriedCertificateEng from './NotRemarriedCertificateEng';
import NotRemarriedCertificateGuj from './NotRemarriedCertificateGuj';
import EstimateOrEvaluationCertificateEnglish from './EstimateOrEvaluationCertificateEnglish';
import EstimateOrEvaluationCertificateGujarati from './EstimateOrEvaluationCertificateGujarati';
import FourboundariesCertificateEnglish from './FourboundariesCertificateEnglish';
import FourboundariesCertificateGujarati from './FourboundariesCertificateGujarati';
import FourBoundariesSurvayCertificateEng from './FourBoundariesSurvayCertificateEng';
import FourBoundariesSurvayCertificateGuj from './FourBoundariesSurvayCertificateGuj';
import NotHaveLandCertificateEnglish from './NotHaveLandCertificateEnglish';
import NotHaveLandCertificateGujarati from './NotHaveLandCertificateGujarati';
import PropertyNearCertificateEng from './PropertyNearCertificateEng';
import PropertyNearCertificateGuj from './PropertyNearCertificateGuj';
import CasteCertificateEng from './CasteCertificateEng';
import CasteCertificateGuj from './CasteCertificateGuj';
import WidowCertificateEng from './WidowCertificateEng';
import WidowCertificateGuj from './WidowCertificateGuj';
export var getCertificate = function (name, data, setleftMargin, setTopMargin) {
    setTopMargin(30);
    switch (name) {
        case 'birthcertificate-en':
            setleftMargin(15);
            return React.createElement(BirthCertificate, tslib_1.__assign({}, data));
        case 'deathcertificate-en':
            setleftMargin(17);
            return React.createElement(DeathCertificate, tslib_1.__assign({}, data));
        case 'bplcertificate-en':
            return React.createElement(BplCertificateEng, tslib_1.__assign({}, data));
        case 'bplcertificate-gu':
            return React.createElement(BplCertificateGujarati, tslib_1.__assign({}, data));
        case 'businesscertificate-en':
            return React.createElement(BusinessCertificateEnglish, tslib_1.__assign({}, data));
        case 'businesscertificate-gu':
            return React.createElement(BusinessCertificateGujarati, tslib_1.__assign({}, data));
        case 'noduescertificate-en':
            return React.createElement(NoDueCertificateEnglish, tslib_1.__assign({}, data));
        case 'noduescertificate-gu':
            return React.createElement(NoDueCertificateGujarati, tslib_1.__assign({}, data));
        case 'residencycertificate-en':
            return React.createElement(ResidencyCertificateEnglish, tslib_1.__assign({}, data));
        case 'residencycertificate-gu':
            return React.createElement(ResidencyCertificateGujarati, tslib_1.__assign({}, data));
        case 'farmercertificate-en':
            return React.createElement(FarmerCertificateEnglish, tslib_1.__assign({}, data));
        case 'farmercertificate-gu':
            return React.createElement(FarmerCertificateGujarati, tslib_1.__assign({}, data));
        case 'stayseparatelycertificate-en':
            return React.createElement(StaySeperatelyCerEng, tslib_1.__assign({}, data));
        case 'stayseparatelycertificate-gu':
            return React.createElement(StaySeperatelyCerGuj, tslib_1.__assign({}, data));
        case 'incomecertificate-en':
            return React.createElement(IncomeCertificateEnglish, tslib_1.__assign({}, data));
        case 'incomecertificate-gu':
            return React.createElement(IncomeCertificateGujarati, tslib_1.__assign({}, data));
        case 'marriagecertificate-en':
            return React.createElement(MarriageCertificateEnglish, tslib_1.__assign({}, data));
        case 'marriagecertificate-gu':
            return React.createElement(MarriageCertificateGujarati, tslib_1.__assign({}, data));
        case 'propertyinsidecertificate-en':
            setleftMargin(60);
            return React.createElement(PropertyInsideCertificateEng, tslib_1.__assign({}, data));
        case 'propertyinsidecertificate-gu':
            setleftMargin(60);
            return React.createElement(PropertyInsideCertificateGuj, tslib_1.__assign({}, data));
        case 'notremarriedcertificate-en':
            setleftMargin(75);
            return React.createElement(NotRemarriedCertificateEng, tslib_1.__assign({}, data));
        case 'notremarriedcertificate-gu':
            setleftMargin(75);
            return React.createElement(NotRemarriedCertificateGuj, tslib_1.__assign({}, data));
        case 'estimateorvaluationcertificate-en':
            return React.createElement(EstimateOrEvaluationCertificateEnglish, tslib_1.__assign({}, data));
        case 'estimateorvaluationcertificate-gu':
            return React.createElement(EstimateOrEvaluationCertificateGujarati, tslib_1.__assign({}, data));
        case 'fourboundariescertificate-en':
            return React.createElement(FourboundariesCertificateEnglish, tslib_1.__assign({}, data));
        case 'fourboundariescertificate-gu':
            return React.createElement(FourboundariesCertificateGujarati, tslib_1.__assign({}, data));
        case 'fourboundariessurveycertificate-en':
            setleftMargin(50);
            return React.createElement(FourBoundariesSurvayCertificateEng, tslib_1.__assign({}, data));
        case 'fourboundariessurveycertificate-gu':
            return React.createElement(FourBoundariesSurvayCertificateGuj, tslib_1.__assign({}, data));
        case 'nothavelandcertificate-en':
            return React.createElement(NotHaveLandCertificateEnglish, tslib_1.__assign({}, data));
        case 'nothavelandcertificate-gu':
            return React.createElement(NotHaveLandCertificateGujarati, tslib_1.__assign({}, data));
        case 'propertynearcertificate-en':
            return React.createElement(PropertyNearCertificateEng, tslib_1.__assign({}, data));
        case 'propertynearcertificate-gu':
            return React.createElement(PropertyNearCertificateGuj, tslib_1.__assign({}, data));
        case 'castecertificate-en':
            return React.createElement(CasteCertificateEng, tslib_1.__assign({}, data));
        case 'castecertificate-gu':
            return React.createElement(CasteCertificateGuj, tslib_1.__assign({}, data));
        case 'widowcertificate-en':
            setleftMargin(70);
            return React.createElement(WidowCertificateEng, tslib_1.__assign({}, data));
        case 'widowcertificate-gu':
            return React.createElement(WidowCertificateGuj, tslib_1.__assign({}, data));
        default:
            return null;
    }
};
//# sourceMappingURL=index.js.map
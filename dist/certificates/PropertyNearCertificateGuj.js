import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
var Default = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  margin: 20px;\n  font-family: \"ChelthmlITC BK BT\";\n"], ["\n  text-align: center;\n  margin: 20px;\n  font-family: \"ChelthmlITC BK BT\";\n"])));
var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\nborder: 1px solid black;\nheight: 800px;\nwidth: 800px;\nmargin: 0 ;\n"], ["\nborder: 1px solid black;\nheight: 800px;\nwidth: 800px;\nmargin: 0 ;\n"])));
var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\nborder: 6px solid black;\nmargin: 0 auto;\nheight: 793px;\nwidth: 793px;\nposition: relative;\ntop: 1.5px;\nleft: 0.3px;\n"], ["\nborder: 6px solid black;\nmargin: 0 auto;\nheight: 793px;\nwidth: 793px;\nposition: relative;\ntop: 1.5px;\nleft: 0.3px;\n"])));
var Heading = styled.div(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n    margin: 0 auto;\n    padding-right: 23px;\n    padding-left: 23px;\n    padding-top:4px;\n    padding-bottom: 15px;\n    text-align: center;\n    position: relative;\n    top:7px;\n    font-size: 20px;\n    border: 3px solid black;\n    width: 250px;\n    font-weight: bold;\n    border-radius: 11px;\n  "], ["\n    margin: 0 auto;\n    padding-right: 23px;\n    padding-left: 23px;\n    padding-top:4px;\n    padding-bottom: 15px;\n    text-align: center;\n    position: relative;\n    top:7px;\n    font-size: 20px;\n    border: 3px solid black;\n    width: 250px;\n    font-weight: bold;\n    border-radius: 11px;\n  "])));
var Container = styled.div(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\nmargin: 0 auto;\nwidth: 180px;\nheight: 180px;\nposition: relative;\ntop:30px;\n"], ["\nmargin: 0 auto;\nwidth: 180px;\nheight: 180px;\nposition: relative;\ntop:30px;\n"])));
var Paras = styled.div(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top:50px;\n  "], ["\n    position: relative;\n    top:50px;\n  "])));
var ParaOne = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n  "], ["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n  "])));
var GrampanchayatName = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 350px;\n    font-family: \"Times New Roman\";\n  "], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 350px;\n    font-family: \"Times New Roman\";\n  "])));
var ParaOneSpan = styled.span(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject([""], [""])));
var ParaTwo = styled.p(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  "], ["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  "])));
var TalukaName = styled.input(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n  "], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n  "])));
var ParaTwoSpan1 = styled.span(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject([""], [""])));
var DistrictName = styled.input(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n  "], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n  "])));
var ParaTwoSpan2 = styled.span(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject([""], [""])));
var ParaThree = styled.p(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n\n  "], ["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n\n  "])));
var PropertyNumber = styled.input(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 18px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n  width: 250px;\n  font-family: \"Times New Roman\";\n  "], ["\n  text-align: center;\n  font-size: 18px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n  width: 250px;\n  font-family: \"Times New Roman\";\n  "])));
var ParaThreeSpan1 = styled.span(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject([""], [""])));
var ParaFour = styled.p(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  right:150px;\n  "], ["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  right:150px;\n  "])));
var ParaThreeSpan2 = styled.span(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject([""], [""])));
var Name = styled.input(templateObject_20 || (templateObject_20 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 220px;\n    font-family: \"Times New Roman\";\n  "], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 220px;\n    font-family: \"Times New Roman\";\n  "])));
var ParaFourSpan = styled.span(templateObject_21 || (templateObject_21 = tslib_1.__makeTemplateObject([""], [""])));
function PropertyNearCertificateGuj(data) {
    var PROPERTY_NUMBER = data.PROPERTY_NUMBER;
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var NAME = data.NAME;
    var DISTRICT_NAME = data.DISTRICT_NAME;
    var PHOTO = data.PHOTO;
    return (React.createElement(Default, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(Heading, null, "\u0AAA\u0ACD\u0AB0\u0AAE\u0ABE\u0AA3\u0AAA\u0AA4\u0ACD\u0AB0 \u0AA8\u0A9C\u0AC0\u0A95 \u0AB8\u0A82\u0AAA\u0AA4\u0ACD\u0AA4\u0ABF"),
                React.createElement(Container, null,
                    React.createElement("img", { src: PHOTO, width: '180px', height: '180px' })),
                React.createElement(Paras, null,
                    React.createElement(ParaOne, null,
                        " ",
                        React.createElement(ParaOneSpan, null, "\u0A86\u0AA5\u0AC0 \u0AA6\u0ABE\u0A96\u0AB2\u0ACB \u0A86\u0AAA\u0AB5\u0ABE\u0AAE\u0ABE \u0A86\u0AB5\u0AC7 \u0A9B\u0AC7 \u0A95\u0AC7 \u0AAE\u0ACB\u0A9C\u0AC7 : "),
                        React.createElement(GrampanchayatName, { value: GRAMPANCHAYAT_NAME })),
                    React.createElement(ParaTwo, null,
                        React.createElement(ParaTwoSpan1, null, "\u0AA4\u0ABE. "),
                        React.createElement(TalukaName, { value: TALUKA_NAME }),
                        React.createElement(ParaTwoSpan2, null, "\u0A9C\u0ABF. "),
                        React.createElement(DistrictName, { value: DISTRICT_NAME }),
                        React.createElement(ParaTwoSpan2, null, "\u0A86\u0AB5\u0AC7\u0AB2 \u0AAE\u0ABF\u0AB2\u0A95\u0AA4  ")),
                    React.createElement(ParaThree, null,
                        "\u0AA8\u0A82 :",
                        React.createElement(PropertyNumber, { value: PROPERTY_NUMBER }),
                        React.createElement(ParaThreeSpan1, null, "\u0A9C\u0AC7\u00A0 "),
                        React.createElement(Name, { value: NAME }),
                        React.createElement(ParaThreeSpan2, null, "\u0AA8\u0ABE \u0AA8\u0ABE\u0AAE\u0AC7 \u0A97\u0ABE\u0AAE\u0AA8\u0AC0 \u0AA8\u0A9C\u0AC0\u0A95 ")),
                    React.createElement(ParaFour, null,
                        React.createElement(ParaFourSpan, null, "\u0A86\u0AB5\u0AC7\u0AB2 \u0A9C\u0AC7 \u0AAC\u0AA6\u0AB2 \u0A86 \u0AA6\u0ABE\u0A96\u0AB2\u0ACB \u0A86\u0AAA\u0AB5\u0ABE\u0AAE\u0ABE \u0A86\u0AB5\u0AC7 \u0A9B\u0AC7.")))))));
}
export default PropertyNearCertificateGuj;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20, templateObject_21;
//# sourceMappingURL=PropertyNearCertificateGuj.js.map
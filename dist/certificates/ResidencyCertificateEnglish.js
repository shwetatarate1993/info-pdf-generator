import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
function ResidencyCertificateEnglish(data) {
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var NAME = data.NAME;
    var AREA = data.AREA;
    var PURPOSE = data.PURPOSE;
    var PLACE = data.PLACE;
    var PHOTO = data.PHOTO;
    var dateString = data.DATE;
    var dateObj = new Date(dateString);
    var momentObj = moment(dateObj);
    var DATE = momentObj.format('YYYY-MMM-DD');
    var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  margin: 20px;\n"], ["\n  text-align: center;\n  margin: 20px;\n"])));
    var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n  border: 1px solid black;\n  height: 800px;\n  width: 800px;\n  margin: 0 ;\n"], ["\n  border: 1px solid black;\n  height: 800px;\n  width: 800px;\n  margin: 0 ;\n"])));
    var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\n  border: 6px solid black;\n  margin: 0 auto;\n  height: 793px;\n  width: 792px;\n  position: relative;\n  top: 1.5px;\n  left: 0.3px;\n"], ["\n  border: 6px solid black;\n  margin: 0 auto;\n  height: 793px;\n  width: 792px;\n  position: relative;\n  top: 1.5px;\n  left: 0.3px;\n"])));
    var NoDueCertificate = styled.div(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n  margin: 0 auto;\n  padding: 30px;\n  text-align: center;\n  position: relative;\n  top: 50px;\n  border: 4px solid black;\n  width: 350px;\n  font-weight: bold;\n  border-radius: 15px;\n  font-size: 30px;\n"], ["\n  margin: 0 auto;\n  padding: 30px;\n  text-align: center;\n  position: relative;\n  top: 50px;\n  border: 4px solid black;\n  width: 350px;\n  font-weight: bold;\n  border-radius: 15px;\n  font-size: 30px;\n"])));
    var Logo = styled.div(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n  width: 150px;\n  height: 200px;\n  position: relative;\n  top: 100px; \n  left: 20px;\n"], ["\n  width: 150px;\n  height: 200px;\n  position: relative;\n  top: 100px; \n  left: 20px;\n"])));
    var Content = styled.div(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n  width: 590px;\n  height: 200px;\n  position: relative;\n  left: 175px;\n  bottom: 105px;\n"], ["\n  width: 590px;\n  height: 200px;\n  position: relative;\n  left: 175px;\n  bottom: 105px;\n"])));
    var ParaOne = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    font-size: 17px;\n    font-weight: bold;\n  "], ["\n    position: relative;\n    font-size: 17px;\n    font-weight: bold;\n  "])));
    var GramPanchayat = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 17px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 17px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaOneTwo = styled.span(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaTwo = styled.p(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 10px;\n  font-size: 17px;\n  font-weight: bold;\n  word-spacing: 2px;\n"], ["\n  position: relative;\n  bottom: 10px;\n  font-size: 17px;\n  font-weight: bold;\n  word-spacing: 2px;\n"])));
    var TalukaName = styled.input(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 210px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 210px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
    var ParaTwoOne = styled.span(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject([""], [""])));
    var Name = styled.input(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 220px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 220px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
    var ParaTwoTwo = styled.span(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaThree = styled.p(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 17px;\n  font-size: 17px;\n  font-weight: bold;\n"], ["\n  position: relative;\n  bottom: 17px;\n  font-size: 17px;\n  font-weight: bold;\n"])));
    var Area = styled.input(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 190px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 190px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
    var ParaThreeOne = styled.span(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject([""], [""])));
    var GramPanchayatOne = styled.input(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 200px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 200px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
    var ParaFour = styled.p(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 22px;\n  font-size: 17px;  \n  font-weight: bold;\n"], ["\n  position: relative;\n  bottom: 22px;\n  font-size: 17px;  \n  font-weight: bold;\n"])));
    var TalukaNameOne = styled.input(templateObject_20 || (templateObject_20 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 190px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 190px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
    var ParaFourOne = styled.span(templateObject_21 || (templateObject_21 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaFive = styled.p(templateObject_22 || (templateObject_22 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 28px;\n  font-size: 17px;\n  font-weight: bold;\n"], ["\n  position: relative;\n  bottom: 28px;\n  font-size: 17px;\n  font-weight: bold;\n"])));
    var Purpose = styled.input(templateObject_23 || (templateObject_23 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 250px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 250px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
    var ParaFiveOne = styled.span(templateObject_24 || (templateObject_24 = tslib_1.__makeTemplateObject([""], [""])));
    var Place = styled.p(templateObject_25 || (templateObject_25 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  top: 50px;\n  right: 330px; \n  font-size: 17px;\n  font-weight: bold;\n"], ["\n  position: relative;\n  top: 50px;\n  right: 330px; \n  font-size: 17px;\n  font-weight: bold;\n"])));
    var PlaceInput = styled.input(templateObject_26 || (templateObject_26 = tslib_1.__makeTemplateObject(["\n  font-size: 17px;\n  font-weight: bold;\n  width: 180px;\n  border: none;\n"], ["\n  font-size: 17px;\n  font-weight: bold;\n  width: 180px;\n  border: none;\n"])));
    var DateOne = styled.p(templateObject_27 || (templateObject_27 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  top: 50px;\n  right: 258px; \n  font-size: 17px;\n  font-weight: bold;\n"], ["\n  position: relative;\n  top: 50px;\n  right: 258px; \n  font-size: 17px;\n  font-weight: bold;\n"])));
    var DateInput = styled.input(templateObject_28 || (templateObject_28 = tslib_1.__makeTemplateObject(["\n  font-size: 17px;\n  font-weight: bold;\n  width: 180px;\n  border: none;\n"], ["\n  font-size: 17px;\n  font-weight: bold;\n  width: 180px;\n  border: none;\n"])));
    var TalatiCumMantri = styled.span(templateObject_29 || (templateObject_29 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  left: 320px;\n"], ["\n  position: relative;\n  left: 320px;\n"])));
    return (React.createElement(App, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(NoDueCertificate, null, "Residency Certificate"),
                React.createElement(Logo, null,
                    React.createElement("img", { src: PHOTO, width: '150', height: '200px' })),
                React.createElement(Content, null,
                    React.createElement(ParaOne, null,
                        "Hence the Gram Panchayat \u00A0\u00A0",
                        React.createElement(GramPanchayat, { value: GRAMPANCHAYAT_NAME }),
                        "\u00A0\u00A0",
                        React.createElement(ParaOneTwo, null, "is certified from")),
                    React.createElement(ParaTwo, null,
                        "Ta. \u00A0",
                        React.createElement(TalukaName, { value: TALUKA_NAME }),
                        "\u00A0",
                        React.createElement(ParaTwoOne, null, "is that,"),
                        "\u00A0",
                        React.createElement(Name, { value: NAME }),
                        "\u00A0",
                        React.createElement(ParaTwoTwo, null, "who")),
                    React.createElement(ParaThree, null,
                        "lives in \u00A0",
                        React.createElement(Area, { value: AREA }),
                        "\u00A0",
                        React.createElement(ParaThreeOne, null, "area in village "),
                        "\u00A0",
                        React.createElement(GramPanchayatOne, { value: GRAMPANCHAYAT_NAME })),
                    React.createElement(ParaFour, null,
                        "(Ta.",
                        React.createElement(TalukaNameOne, { value: TALUKA_NAME }),
                        React.createElement(ParaFourOne, null, "). The fact is true and correct.This certificates")),
                    React.createElement(ParaFive, null,
                        "given only for the purpose \u00A0",
                        React.createElement(Purpose, { value: PURPOSE }),
                        "\u00A0",
                        React.createElement(ParaFiveOne, null, "of going out.")),
                    React.createElement(Place, null,
                        "Place : \u00A0",
                        React.createElement(PlaceInput, { value: PLACE })),
                    React.createElement(DateOne, null,
                        "Date : \u00A0",
                        React.createElement(DateInput, { value: DATE }),
                        "\u00A0",
                        React.createElement(TalatiCumMantri, null, "Talati Cum Mantri")))))));
}
export default ResidencyCertificateEnglish;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20, templateObject_21, templateObject_22, templateObject_23, templateObject_24, templateObject_25, templateObject_26, templateObject_27, templateObject_28, templateObject_29;
//# sourceMappingURL=ResidencyCertificateEnglish.js.map
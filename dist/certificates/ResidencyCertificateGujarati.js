import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
function ResidencyCertificateGujarati(data) {
    var TALUKA_NAME = data.TALUKA_NAME;
    var AREA = data.AREA;
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var NAME = data.NAME;
    var PHOTO = data.PHOTO;
    var PLACE = data.PLACE;
    var PURPOSE = data.PURPOSE;
    var dateString = data.DATE;
    var dateObj = new Date(dateString);
    var momentObj = moment(dateObj);
    var DATE = momentObj.format('YYYY-MMM-DD');
    var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  margin: 20px;\n"], ["\n  text-align: center;\n  margin: 20px;\n"])));
    var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n  border: 1px solid black;\n  height: 800px;\n  width: 800px;\n  margin: 0 ;\n"], ["\n  border: 1px solid black;\n  height: 800px;\n  width: 800px;\n  margin: 0 ;\n"])));
    var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\n  border: 6px solid black;\n  margin: 0 auto;\n  height: 793px;\n  width: 792px;\n  position: relative;\n  top: 1.5px;\n  left: 0.3px;\n"], ["\n  border: 6px solid black;\n  margin: 0 auto;\n  height: 793px;\n  width: 792px;\n  position: relative;\n  top: 1.5px;\n  left: 0.3px;\n"])));
    var ResidencyCertificate = styled.div(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n  margin: 0 auto;\n  padding: 30px;\n  text-align: center;\n  position: relative;\n  top: 50px;\n  border: 4px solid black;\n  width: 350px;\n  font-weight: bold;\n  border-radius: 15px;\n  font-size: 30px;\n"], ["\n  margin: 0 auto;\n  padding: 30px;\n  text-align: center;\n  position: relative;\n  top: 50px;\n  border: 4px solid black;\n  width: 350px;\n  font-weight: bold;\n  border-radius: 15px;\n  font-size: 30px;\n"])));
    var Logo = styled.div(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n  width: 150px;\n  height: 200px;\n  position: relative;\n  top: 100px; \n  left: 20px;\n"], ["\n  width: 150px;\n  height: 200px;\n  position: relative;\n  top: 100px; \n  left: 20px;\n"])));
    var Content = styled.div(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n  width: 590px;\n  height: 200px;\n  position: relative;\n  left: 175px;\n  bottom: 105px;\n"], ["\n  width: 590px;\n  height: 200px;\n  position: relative;\n  left: 175px;\n  bottom: 105px;\n"])));
    var ParaOne = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    font-size: 17px;\n    font-weight: bold;\n  "], ["\n    position: relative;\n    font-size: 17px;\n    font-weight: bold;\n  "])));
    var GramPanchayatOne = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 17px;\n    font-weight: bold;\n    width: 200px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 17px;\n    font-weight: bold;\n    width: 200px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaOneTwo = styled.span(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject([""], [""])));
    var TalukaInputOne = styled.input(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 17px;\n    font-weight: bold;\n    width: 200px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n "], ["\n    text-align: center;\n    font-size: 17px;\n    font-weight: bold;\n    width: 200px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n "])));
    var ParaTwo = styled.p(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 5px;\n  font-size: 17px;\n  font-weight: bold;\n"], ["\n  position: relative;\n  bottom: 5px;\n  font-size: 17px;\n  font-weight: bold;\n"])));
    var Name = styled.input(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 250px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 250px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
    var ParaTwoOne = styled.span(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaThree = styled.p(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 13px;\n  right: 6px;\n  font-size: 17px;\n  font-weight: bold;\n  word-spacing: 13px;\n"], ["\n  position: relative;\n  bottom: 13px;\n  right: 6px;\n  font-size: 17px;\n  font-weight: bold;\n  word-spacing: 13px;\n"])));
    var GramPanchayatTwo = styled.input(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
    var ParaThreeOne = styled.span(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject([""], [""])));
    var TalukaInputTwo = styled.input(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
    var ParaThreeTwo = styled.span(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaFour = styled.p(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 20px;\n  font-size: 17px;  \n  font-weight: bold;\n  word-spacing: 3px;\n"], ["\n  position: relative;\n  bottom: 20px;\n  font-size: 17px;  \n  font-weight: bold;\n  word-spacing: 3px;\n"])));
    var Area = styled.input(templateObject_20 || (templateObject_20 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 200px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 200px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
    var ParaFourOne = styled.span(templateObject_21 || (templateObject_21 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaFive = styled.p(templateObject_22 || (templateObject_22 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  bottom: 25px;\n  font-size: 17px;\n  font-weight: bold;\n  word-spacing: 3.5px;\n"], ["\n  position: relative;\n  bottom: 25px;\n  font-size: 17px;\n  font-weight: bold;\n  word-spacing: 3.5px;\n"])));
    var Purpose = styled.input(templateObject_23 || (templateObject_23 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 250px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 17px;\n  font-weight: bold;\n  width: 250px;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
    var ParaFiveOne = styled.span(templateObject_24 || (templateObject_24 = tslib_1.__makeTemplateObject([""], [""])));
    var Place = styled.p(templateObject_25 || (templateObject_25 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  top: 50px;\n  right: 330px; \n  font-size: 17px;\n  font-weight: bold;\n"], ["\n  position: relative;\n  top: 50px;\n  right: 330px; \n  font-size: 17px;\n  font-weight: bold;\n"])));
    var PlaceInput = styled.input(templateObject_26 || (templateObject_26 = tslib_1.__makeTemplateObject(["\n  font-size: 17px;\n  font-weight: bold;\n  width: 180px;\n  border: none;\n"], ["\n  font-size: 17px;\n  font-weight: bold;\n  width: 180px;\n  border: none;\n"])));
    var DateOne = styled.p(templateObject_27 || (templateObject_27 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  top: 50px;\n  right: 268px; \n  font-size: 17px;\n  font-weight: bold;\n"], ["\n  position: relative;\n  top: 50px;\n  right: 268px; \n  font-size: 17px;\n  font-weight: bold;\n"])));
    var DateInput = styled.input(templateObject_28 || (templateObject_28 = tslib_1.__makeTemplateObject(["\n  font-size: 17px;\n  font-weight: bold;\n  width: 180px;\n  border: none;\n"], ["\n  font-size: 17px;\n  font-weight: bold;\n  width: 180px;\n  border: none;\n"])));
    var TalatiCumMantri = styled.span(templateObject_29 || (templateObject_29 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  left: 340px;\n"], ["\n  position: relative;\n  left: 340px;\n"])));
    return (React.createElement(App, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(ResidencyCertificate, null, "\u0AB0\u0AB9\u0AC0\u0AB6\u0AA8\u0ACB \u0AA6\u0ABE\u0A96\u0AB2\u0ACB"),
                React.createElement(Logo, null,
                    React.createElement("img", { src: PHOTO, width: '150px', height: '200px' })),
                React.createElement(Content, null,
                    React.createElement(ParaOne, null,
                        "\u0A86\u0AA5\u0AC0 \u0A97\u0ACD\u0AB0\u0ABE\u0AAE \u0AAA\u0A82\u0A9A\u0ABE\u0AAF\u0AA4 \u00A0",
                        React.createElement(GramPanchayatOne, { value: GRAMPANCHAYAT_NAME }),
                        "\u00A0",
                        React.createElement(ParaOneTwo, null, "\u0AA4\u0ABE."),
                        "\u00A0",
                        React.createElement(TalukaInputOne, { value: TALUKA_NAME })),
                    React.createElement(ParaTwo, null,
                        "\u0AA4\u0AB0\u0AAB\u0AA5\u0AC0 \u0AA6\u0ABE\u0A96\u0AB2\u0ACB \u0A86\u0AAA\u0AB5\u0ABE\u0AAE\u0ABE \u0A86\u0AB5\u0AC7 \u0A9B\u0AC7 \u0A95\u0AC7, \u0AB6\u0ACD\u0AB0\u0AC0 \u00A0",
                        React.createElement(Name, { value: NAME }),
                        "\u00A0",
                        React.createElement(ParaTwoOne, null, "\u0A9C\u0AC7\u0A93")),
                    React.createElement(ParaThree, null,
                        React.createElement(GramPanchayatTwo, { value: GRAMPANCHAYAT_NAME }),
                        "\u00A0",
                        React.createElement(ParaThreeOne, null, "(\u0AA4\u0ABE."),
                        "\u00A0",
                        React.createElement(TalukaInputTwo, { value: TALUKA_NAME }),
                        "\u00A0",
                        React.createElement(ParaThreeTwo, null, ") \u0A97\u0ABE\u0AAE\u0AC7 ")),
                    React.createElement(ParaFour, null,
                        React.createElement(Area, { value: AREA }),
                        "\u00A0",
                        React.createElement(ParaFourOne, null, "\u0AB5\u0ABF\u0AB8\u0ACD\u0AA4\u0ABE\u0AB0\u0AAE\u0ABE\u0A82 \u0AB0\u0AB9\u0AC7 \u0A9B\u0AC7. \u0A9C\u0AC7 \u0AB9\u0A95\u0AC0\u0A95\u0AA4 \u0A96\u0AB0\u0AC0 \u0A85\u0AA8\u0AC7 \u0AAC\u0AB0\u0ACB\u0AAC\u0AB0 \u0A9B\u0AC7.")),
                    React.createElement(ParaFive, null,
                        "\u0AB8\u0AA6\u0AB0 \u0AA6\u0ABE\u0A96\u0AB2\u0ACB \u0AAB\u0A95\u0ACD\u0AA4 \u00A0",
                        React.createElement(Purpose, { value: PURPOSE }),
                        "\u00A0",
                        React.createElement(ParaFiveOne, null, "\u0AA8\u0ABE \u0AB9\u0AC7\u0AA4\u0AC1 \u0A85\u0AB0\u0ACD\u0AA5\u0AC7 \u0A86\u0AAA\u0AC7\u0AB2 \u0A9B\u0AC7.")),
                    React.createElement(Place, null,
                        "\u0AB8\u0ACD\u0AA5\u0AB3 : \u00A0",
                        React.createElement(PlaceInput, { value: PLACE })),
                    React.createElement(DateOne, null,
                        "\u0AA4\u0ABE\u0AB0\u0AC0\u0A96 : \u00A0",
                        React.createElement(DateInput, { value: DATE }),
                        "\u00A0",
                        React.createElement(TalatiCumMantri, null, "\u0AA4\u0AB2\u0ABE\u0A9F\u0AC0\u0A95\u0AAE \u0AAE\u0A82\u0AA4\u0ACD\u0AB0\u0AC0")))))));
}
export default ResidencyCertificateGujarati;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20, templateObject_21, templateObject_22, templateObject_23, templateObject_24, templateObject_25, templateObject_26, templateObject_27, templateObject_28, templateObject_29;
//# sourceMappingURL=ResidencyCertificateGujarati.js.map
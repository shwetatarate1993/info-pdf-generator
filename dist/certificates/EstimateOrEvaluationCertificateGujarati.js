import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    margin: 20px;\n  "], ["\n    text-align: center;\n    margin: 20px;\n  "])));
var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n  border: 1px solid black;\n  height: 800px;\n  width: 800px;\n  margin: 0 ;\n"], ["\n  border: 1px solid black;\n  height: 800px;\n  width: 800px;\n  margin: 0 ;\n"])));
var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\nborder: 6px solid black;\nmargin: 0 auto;\nheight: 793px;\nwidth: 793px;\nposition: relative;\ntop: 1.5px;\nleft: 0.3px;\n"], ["\nborder: 6px solid black;\nmargin: 0 auto;\nheight: 793px;\nwidth: 793px;\nposition: relative;\ntop: 1.5px;\nleft: 0.3px;\n"])));
var EstimateCertificate = styled.div(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\nmargin: 0 auto;\npadding: 30px;\ntext-align: center;\nposition: relative;\ntop: 10px;\nborder: 3px solid black;\nwidth: 400px;\nfont-weight: bold;\nborder-radius: 15px;\nfont-size: 30px;\n"], ["\nmargin: 0 auto;\npadding: 30px;\ntext-align: center;\nposition: relative;\ntop: 10px;\nborder: 3px solid black;\nwidth: 400px;\nfont-weight: bold;\nborder-radius: 15px;\nfont-size: 30px;\n"])));
var ParaOne = styled.p(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 20px;\n    left: 100px;\n    font-size: 20px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width : 550px;\n  "], ["\n    position: relative;\n    top: 20px;\n    left: 100px;\n    font-size: 20px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width : 550px;\n  "])));
var GramPanchayat1 = styled.input(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid white;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid white;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
var ParaTwo = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 200px;\n  left: 80px;\n  font-size: 20px;\n"], ["\n  position: absolute;\n  top: 200px;\n  left: 80px;\n  font-size: 20px;\n"])));
var GramPanchayat2 = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
var ParaThree = styled.p(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 250px;\n  left: 80px;\n  font-size: 20px;\n"], ["\n  position: absolute;\n  top: 250px;\n  left: 80px;\n  font-size: 20px;\n"])));
var DateInput = styled.input(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\nfont-size: 20px;\nfont-weight: bold;\nwidth: 150px;\ntext-align: center;\nborder-bottom: 1.5px solid black;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\n"], ["\nfont-size: 20px;\nfont-weight: bold;\nwidth: 150px;\ntext-align: center;\nborder-bottom: 1.5px solid black;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\n"])));
var Taluka = styled.input(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\ntext-align: center;\nfont-size: 20px;\nfont-weight: bold;\nwidth: 230px;\nborder-bottom: 1.5px solid black;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\n"], ["\ntext-align: center;\nfont-size: 20px;\nfont-weight: bold;\nwidth: 230px;\nborder-bottom: 1.5px solid black;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\n"])));
var ParaFour = styled.p(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 300px;\n  left: 80px;\n  font-size: 20px;\n"], ["\n  position: absolute;\n  top: 300px;\n  left: 80px;\n  font-size: 20px;\n"])));
var Name = styled.input(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
var ParaFive = styled.p(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 350px;\n  left: 80px;\n  font-size: 20px;\n"], ["\n  position: absolute;\n  top: 350px;\n  left: 80px;\n  font-size: 20px;\n"])));
var TypeOfBusiness = styled.input(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
var ParaSix = styled.p(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 400px;\n  left: 80px;\n  font-size: 20px;\n  word-spacing: 8px;\n"], ["\n  position: absolute;\n  top: 400px;\n  left: 80px;\n  font-size: 20px;\n  word-spacing: 8px;\n"])));
var ParaSeven = styled.p(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 450px;\n  left: 80px;\n  font-size: 20px;\n"], ["\n  position: absolute;\n  top: 450px;\n  left: 80px;\n  font-size: 20px;\n"])));
var Amount = styled.input(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n  width:250px;\n"], ["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n  width:250px;\n"])));
var AmountInWords = styled.input(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
var ParaEight = styled.p(templateObject_20 || (templateObject_20 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 500px;\n  left: 80px;\n  font-size: 20px;\n"], ["\n  position: absolute;\n  top: 500px;\n  left: 80px;\n  font-size: 20px;\n"])));
var DateOne = styled.p(templateObject_21 || (templateObject_21 = tslib_1.__makeTemplateObject(["\nposition: absolute;\ntop: 550px;\nleft: 80px;\nfont-size: 20px;\nfont-weight: bold;\nword-spacing: 8px;\n"], ["\nposition: absolute;\ntop: 550px;\nleft: 80px;\nfont-size: 20px;\nfont-weight: bold;\nword-spacing: 8px;\n"])));
var DateInput1 = styled.input(templateObject_22 || (templateObject_22 = tslib_1.__makeTemplateObject(["\nfont-size: 20px;\nfont-weight: bold;\nwidth: 150px;\nborder: none;\n"], ["\nfont-size: 20px;\nfont-weight: bold;\nwidth: 150px;\nborder: none;\n"])));
var ParaNine = styled.p(templateObject_23 || (templateObject_23 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 590px;\n  left: 80px;\n  font-size: 20px;\n  font-weight: bold;\n"], ["\n  position: absolute;\n  top: 590px;\n  left: 80px;\n  font-size: 20px;\n  font-weight: bold;\n"])));
var ParaTen = styled.p(templateObject_24 || (templateObject_24 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 630px;\n  left: 80px;\n  font-size: 20px;\n  font-weight: bold;\n"], ["\n  position: absolute;\n  top: 630px;\n  left: 80px;\n  font-size: 20px;\n  font-weight: bold;\n"])));
var ParaEleven = styled.p(templateObject_25 || (templateObject_25 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 670px;\n  left: 80px;\n  font-size: 20px;\n"], ["\n  position: absolute;\n  top: 670px;\n  left: 80px;\n  font-size: 20px;\n"])));
var GramPanchayat3 = styled.input(templateObject_26 || (templateObject_26 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
function EstimateOrEvaluationCertificateGujarati(data) {
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var NAME = data.NAME;
    var TYPE_OF_BUSINESS = data.TYPE_OF_BUSINESS;
    var AMOUNT = data.AMOUNT;
    var AMOUNT_IN_WORD = data.AMOUNT_IN_WORD;
    var dateString = data.DATE;
    var dateObjOne = new Date(dateString);
    var momentObj = moment(dateObjOne);
    var DATE = momentObj.format('YYYY-MMM-DD');
    return (React.createElement(App, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(ParaOne, null,
                    React.createElement(GramPanchayat1, { value: GRAMPANCHAYAT_NAME }),
                    "\u0A97\u0ACD\u0AB0\u0ABE\u0AAE \u0AAA\u0A82\u0A9A\u0ABE\u0AAF\u0AA4"),
                React.createElement(EstimateCertificate, null, "\u0AAA\u0A82\u0A9A\u0A95\u0ABE\u0AAF\u0AB8"),
                React.createElement(ParaTwo, null,
                    "\u0A85\u0AAE\u0ACB \u0AA8\u0AC0\u0A9A\u0AC7 \u0AB8\u0AB9\u0AC0\u0A93 \u0A95\u0AB0\u0AA8\u0ABE\u0AB0 \u0AAA\u0A82\u0A9A\u0ACB \u0AB0\u0AC7.  \u0A97\u0ACD\u0AB0\u0ABE\u0AAE:- ",
                    React.createElement(GramPanchayat2, { value: GRAMPANCHAYAT_NAME })),
                React.createElement(ParaThree, null,
                    "\u0AA4\u0ABE:-",
                    React.createElement(Taluka, { value: TALUKA_NAME }),
                    "\u0AA8\u0ABE \u0A86\u0A9C \u0AB0\u0ACB\u0A9C \u0AA4\u0ABE\u0AB0\u0AC0\u0A96- ",
                    React.createElement(DateInput, { value: DATE }),
                    " "),
                React.createElement(ParaFour, null,
                    "\u0AA8\u0ABE \u0AB0\u0ACB\u0A9C \u0AB9\u0ABE\u0A9C\u0AB0 \u0AA5\u0A87 \u0AB2\u0A96\u0ABE\u0AB5\u0AC0\u0A8F \u0A9B\u0AC0\u0A8F \u0A95\u0AC7 \u0AB6\u0ACD\u0AB0\u0AC0 ",
                    React.createElement(Name, { value: NAME })),
                React.createElement(ParaFive, null,
                    "\u0A85\u0AAE\u0ABE\u0AB0\u0ABE \u0A97\u0ABE\u0AAE\u0AA8\u0ABE \u0AB0\u0AB9\u0AC0\u0AB6 \u0A9B\u0AC7 \u0A85\u0AA8\u0AC7 \u0AA4\u0AC7\u0A93 \u0A97\u0ABE\u0AAE\u0AC7 ",
                    React.createElement(TypeOfBusiness, { value: TYPE_OF_BUSINESS })),
                React.createElement(ParaSix, null,
                    "\u0A95\u0AB0\u0AC7 \u0A9B\u0AC7 \u0A85\u0AA8\u0AC7 \u0A9C\u0AC7\u0A93\u0AA8\u0AC0 \u0AB5\u0ABE\u0AB0\u0ACD\u0AB7\u0ABF\u0A95 \u0A86\u0AB5\u0A95 \u0AB0\u0AC2 ",
                    React.createElement(Amount, { value: AMOUNT })),
                React.createElement(ParaSeven, null,
                    "/- \u0A85\u0A82\u0A95\u0AC7  ",
                    React.createElement(AmountInWords, { value: AMOUNT_IN_WORD }),
                    "\u0A95\u0AB0\u0AA4\u0ABE \u0AB5\u0AA7\u0ABE\u0AB0\u0AC7 \u0AA5\u0AA4\u0AC0 \u0AA8\u0AA5\u0AC0. \u0A9C\u0AC7\u0AA8\u0AC0 \u0A96\u0ABE\u0AA4\u0ACD\u0AB0\u0AC0 "),
                React.createElement(ParaEight, null, "\u0A95\u0AB0\u0AC0 \u0A86 \u0AAA\u0A82\u0A9A\u0A95\u0AAF\u0ABE\u0AB8 \u0AB2\u0A96\u0ABE\u0AB5\u0AC0\u0A8F \u0A9B\u0AC0\u0A8F."),
                React.createElement(DateOne, null,
                    "\u0AA4\u0ABE\u0AB0\u0AC0\u0A96: ",
                    React.createElement(DateInput1, { value: DATE }),
                    "  \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 -------------------"),
                React.createElement(ParaNine, null, "\u0AB0\u0AC2\u0AAC\u0AB0\u0AC2  \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0-------------------"),
                React.createElement(ParaTen, null, "\u0AA4\u0AB2\u0ABE\u0A9F\u0AC0 \u0A95\u0AAE \u0AAE\u0A82\u0AA4\u0ACD\u0AB0\u0AC0 \u0AB6\u0ACD\u0AB0\u0AC0  \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0-------------------"),
                React.createElement(ParaEleven, null,
                    React.createElement(GramPanchayat3, { value: GRAMPANCHAYAT_NAME }))))));
}
export default EstimateOrEvaluationCertificateGujarati;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20, templateObject_21, templateObject_22, templateObject_23, templateObject_24, templateObject_25, templateObject_26;
//# sourceMappingURL=EstimateOrEvaluationCertificateGujarati.js.map
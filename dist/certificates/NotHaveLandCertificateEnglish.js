import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    margin: 20px;\n  "], ["\n    text-align: center;\n    margin: 20px;\n  "])));
var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n  border: 1px solid black;\n  height: 800px;\n  width: 800px;\n  margin: 0 ;\n"], ["\n  border: 1px solid black;\n  height: 800px;\n  width: 800px;\n  margin: 0 ;\n"])));
var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\nborder: 6px solid black;\nmargin: 0 auto;\nheight: 792px;\nwidth: 791px;\nposition: relative;\ntop: 1.5px;\nleft: 0.3px;\n"], ["\nborder: 6px solid black;\nmargin: 0 auto;\nheight: 792px;\nwidth: 791px;\nposition: relative;\ntop: 1.5px;\nleft: 0.3px;\n"])));
var NotHaveLandCertificate = styled.div(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n    margin: 0 auto;\n    padding: 30px;\n    text-align: center;\n    position: relative;\n    top: 50px;\n    border: 4px solid black;\n    width: 450px;\n    font-weight: bold;\n    border-radius: 15px;\n    font-size: 30px;\n  "], ["\n    margin: 0 auto;\n    padding: 30px;\n    text-align: center;\n    position: relative;\n    top: 50px;\n    border: 4px solid black;\n    width: 450px;\n    font-weight: bold;\n    border-radius: 15px;\n    font-size: 30px;\n  "])));
var ParaOne = styled.p(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 80px;\n    left: 20px;\n    font-size: 20px;\n  "], ["\n    position: relative;\n    top: 80px;\n    left: 20px;\n    font-size: 20px;\n  "])));
var Name = styled.input(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
var ParaTwo = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 70px;\n    font-size: 20px;\n  "], ["\n    position: relative;\n    top: 70px;\n    font-size: 20px;\n  "])));
var GramPanchayat = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width:290px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width:290px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
var Taluka = styled.input(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 290px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 290px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
var ParaThree = styled.p(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 70px;\n    font-size: 20px;\n    word-spacing: 5px;\n  "], ["\n    position: relative;\n    top: 70px;\n    font-size: 20px;\n    word-spacing: 5px;\n  "])));
function NotHaveLandCertificateEnglish(data) {
    var NAME = data.NAME;
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    return (React.createElement(App, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(NotHaveLandCertificate, null, "Not Have Any Land Certificate"),
                React.createElement(ParaOne, null,
                    "This is to certificate that given to: ",
                    React.createElement(Name, { value: NAME }),
                    "is a resident "),
                React.createElement(ParaTwo, null,
                    "of Moj.    ",
                    React.createElement(GramPanchayat, { value: GRAMPANCHAYAT_NAME }),
                    " Ta. ",
                    React.createElement(Taluka, { value: TALUKA_NAME }),
                    " "),
                React.createElement(ParaThree, null, "who do not have any farming land.For this, the certificate is given.")))));
}
export default NotHaveLandCertificateEnglish;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10;
//# sourceMappingURL=NotHaveLandCertificateEnglish.js.map
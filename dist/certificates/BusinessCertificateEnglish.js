import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
function BusinessCertificateEnglish(data) {
    var NAME = data.NAME;
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var DISTRICT_NAME = data.DISTRICT_NAME;
    var BUSINESS_NAME = data.BUSINESS_NAME;
    var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    margin: 20px;\n  "], ["\n    text-align: center;\n    margin: 20px;\n  "])));
    var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n    border: 1px solid black;\n    height: 800px;\n    width: 800px;\n    margin: 0 ;\n  "], ["\n    border: 1px solid black;\n    height: 800px;\n    width: 800px;\n    margin: 0 ;\n  "])));
    var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 793px;\n    width: 793px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "], ["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 793px;\n    width: 793px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "])));
    var BusinessCertificate = styled.div(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n    margin: 0 auto;\n    padding: 30px;\n    text-align: center;\n    position: relative;\n    top: 50px;\n    border: 4px solid black;\n    width: 300px;\n    font-weight: bold;\n    border-radius: 15px;\n    font-size: 30px;\n  "], ["\n    margin: 0 auto;\n    padding: 30px;\n    text-align: center;\n    position: relative;\n    top: 50px;\n    border: 4px solid black;\n    width: 300px;\n    font-weight: bold;\n    border-radius: 15px;\n    font-size: 30px;\n  "])));
    var ParaOne = styled.p(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 80px;\n    left: 20px;\n    font-size: 20px;\n    font-weight: bold;\n  "], ["\n    position: relative;\n    top: 80px;\n    left: 20px;\n    font-size: 20px;\n    font-weight: bold;\n  "])));
    var Name = styled.input(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaTwo = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 80px;\n    font-size: 20px;\n    font-weight: bold;\n  "], ["\n    position: relative;\n    top: 80px;\n    font-size: 20px;\n    font-weight: bold;\n  "])));
    var GramPanchayatName = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 270px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 270px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var Taluka = styled.span(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject([""], [""])));
    var TalukaName = styled.input(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 275px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 275px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var District = styled.p(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 80px;\n    font-size: 20px;\n    font-weight: bold;\n  "], ["\n    position: relative;\n    top: 80px;\n    font-size: 20px;\n    font-weight: bold;\n  "])));
    var DistrictName = styled.input(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 200px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 200px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var Business = styled.span(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject([""], [""])));
    var BusinessName = styled.input(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 200px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 200px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    return (React.createElement(App, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(BusinessCertificate, null, "Business Certificate"),
                React.createElement(ParaOne, null,
                    "This certification is provided to Mr. /Ms. \u00A0\u00A0",
                    React.createElement(Name, { value: NAME })),
                React.createElement(ParaTwo, null,
                    "resides in \u00A0\u00A0",
                    React.createElement(GramPanchayatName, { value: GRAMPANCHAYAT_NAME }),
                    "\u00A0\u00A0",
                    React.createElement(Taluka, null, "Ta."),
                    "\u00A0\u00A0",
                    React.createElement(TalukaName, { value: TALUKA_NAME })),
                React.createElement(District, null,
                    "Dist. \u00A0\u00A0",
                    React.createElement(DistrictName, { value: DISTRICT_NAME }),
                    "\u00A0\u00A0",
                    React.createElement(Business, null, "for acquiring  the business"),
                    "\u00A0\u00A0",
                    React.createElement(BusinessName, { value: BUSINESS_NAME }),
                    ".")))));
}
export default BusinessCertificateEnglish;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14;
//# sourceMappingURL=BusinessCertificateEnglish.js.map
import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
function WidowCertificateGuj(data) {
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var DISTRICT_NAME = data.DISTRICT_NAME;
    var NAME = data.NAME;
    var HUSBAND_NAME = data.HUSBAND_NAME;
    var dateString = data.DATE;
    var dateObj = new Date(dateString);
    var momentObj = moment(dateObj);
    var DATE = momentObj.format('YYYY-MMM-DD');
    var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n text-align: left;\n margin: 30px;\n"], ["\n text-align: left;\n margin: 30px;\n"])));
    var BorderA = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\nborder: 1px solid black;\nheight: 472px;\nwidth: 772px;\nmargin: 0 ;\n"], ["\nborder: 1px solid black;\nheight: 472px;\nwidth: 772px;\nmargin: 0 ;\n"])));
    var BorderB = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\nborder: 6px solid black;\nmargin: 0 auto;\nheight: 466px;\nwidth: 766px;\nposition: relative;\ntop: 1.5px;\nleft: 0.3px;\n"], ["\nborder: 6px solid black;\nmargin: 0 auto;\nheight: 466px;\nwidth: 766px;\nposition: relative;\ntop: 1.5px;\nleft: 0.3px;\n"])));
    var Header = styled.p(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\nmargin: 0 auto;\npadding: 10px;\ntext-align: center;\nposition: relative;\ntop: 20px;\nborder: 4px solid black;\nwidth: 350px;\nfont-weight: bold;\nborder-radius: 15px;\nfont-size: 30px;\n"], ["\nmargin: 0 auto;\npadding: 10px;\ntext-align: center;\nposition: relative;\ntop: 20px;\nborder: 4px solid black;\nwidth: 350px;\nfont-weight: bold;\nborder-radius: 15px;\nfont-size: 30px;\n"])));
    var ParaOne = styled.p(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n   position: relative;\n   top: 80px;\n   left: 30px;\n   right:20px;\n   font-size: 20px;\n   font-weight: bold;\n   margin-right: 30px;\n   line-height: 40px;\n "], ["\n   position: relative;\n   top: 80px;\n   left: 30px;\n   right:20px;\n   font-size: 20px;\n   font-weight: bold;\n   margin-right: 30px;\n   line-height: 40px;\n "])));
    var Name = styled.input(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:90px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:90px;\n"])));
    var ParaOneSpan = styled.span(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject([""], [""])));
    var HusbandName = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:140px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:140px;\n"])));
    var DateOne = styled.input(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject(["\nfont-weight: bold;\nfont-size: 16px;\nborder-bottom: 1.5px solid black;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\ntext-align: center;\nwidth:150px;\n "], ["\nfont-weight: bold;\nfont-size: 16px;\nborder-bottom: 1.5px solid black;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\ntext-align: center;\nwidth:150px;\n "])));
    var GrampanchayatName = styled.input(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n"])));
    var TalukaName = styled.input(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:140px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:140px;\n"])));
    var DistrictName = styled.input(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:140px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:140px;\n"])));
    var SarpanchShri = styled.label(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n font-size: 20px;\n font-weight: bold;\n position: relative;\n text-align : left;\n top:130px;\n left: 30px;\n "], ["\n font-size: 20px;\n font-weight: bold;\n position: relative;\n text-align : left;\n top:130px;\n left: 30px;\n "])));
    var TalatiCumMantri = styled.label(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject(["\n position: relative;\n top: 130px;\n font-size: 20px;\n font-weight: bold;\n left:510px;\n "], ["\n position: relative;\n top: 130px;\n font-size: 20px;\n font-weight: bold;\n left:510px;\n "])));
    return (React.createElement(App, null,
        React.createElement(BorderA, { id: 'download-div' },
            React.createElement(BorderB, null,
                React.createElement(Header, null, "\u0AB5\u0ABF\u0AA7\u0AB5\u0ABE \u0AAA\u0ACD\u0AB0\u0AAE\u0ABE\u0AA3\u0AAA\u0AA4\u0ACD\u0AB0"),
                React.createElement(ParaOne, null,
                    "\u0A86\u0AA5\u0AC0 \u00A0\u00A0\u0AA6\u0ABE\u0A96\u0AB2\u0ACB \u00A0\u00A0\u0A86\u0AAA\u0AB5\u0ABE\u0AAE\u0ABE \u00A0\u00A0\u0A86\u0AB5\u0AC7\u00A0\u00A0 \u0A9B\u0AC7 \u00A0\u00A0\u0A95\u0AC7\u00A0\u00A0 \u0AAE\u0ACB\u0A9C\u0AC7 : \u00A0\u00A0",
                    React.createElement(GrampanchayatName, { value: GRAMPANCHAYAT_NAME }),
                    "\u00A0\u00A0",
                    React.createElement(ParaOneSpan, null, "\u00A0\u00A0 \u0AA4\u0ABE. \u00A0\u00A0"),
                    "\u00A0\u00A0",
                    React.createElement(TalukaName, { value: TALUKA_NAME }),
                    " \u00A0\u00A0 \u0A9C\u0AC0. \u00A0\u00A0",
                    React.createElement(DistrictName, { value: DISTRICT_NAME }),
                    " \u00A0\u00A0 \u0AA8\u0ABE \u00A0\u00A0\u0AB0\u0AB9\u0AC0\u0AB6 \u00A0\u00A0 \u0AB6\u0ACD\u0AB0\u0AC0 \u00A0\u00A0 \u0A9C\u0AC7  \u00A0\u00A0 \u00A0\u00A0",
                    React.createElement(Name, { value: NAME }),
                    " \u00A0\u00A0 \u0A9C\u0AC7\u0A93\u0AA8\u0ABE \u0AAA\u0AA4\u0ABF \u00A0\u00A0",
                    React.createElement(HusbandName, { value: HUSBAND_NAME }),
                    " \u00A0\u00A0 \u0AA4\u0ABE. \u00A0\u00A0",
                    React.createElement(DateOne, { value: DATE }),
                    React.createElement(ParaOneSpan, null, " \u00A0\u00A0\u0AA8\u0ABE \u0AB0\u0ACB\u0A9C \u0AAE\u0AB0\u0AA3 \u0AAA\u0ABE\u0AAE\u0AC7\u0AB2 \u0A9B\u0AC7 . \u0A9C\u0AC7\u0AA5\u0AC0 \u0AA4\u0AC7\u0A93 \u0AA4\u0ACD\u0AAF\u0ABE\u0AB0\u0AA5\u0AC0 \u0AB5\u0ABF\u0AA7\u0AB5\u0ABE \u0AA5\u0AAF\u0AC7\u0AB2 \u0A9B\u0AC7. \u0A9C\u0AC7 \u0A96\u0ABE\u0AA4\u0ACD\u0AB0\u0AC0 \u0A95\u0AB0\u0AC0 \u0A86 \u0AA6\u0ABE\u0A96\u0AB2\u0ACB \u0A86\u0AAA\u0AB5\u0ABE\u0AAE\u0ABE \u0A86\u0AB5\u0AC7 \u0A9B\u0AC7."),
                    "\u00A0\u00A0"),
                React.createElement(SarpanchShri, null, "\u0AB8\u0AB0\u0AAA\u0A82\u0A9A \u0AB6\u0ACD\u0AB0\u0AC0"),
                React.createElement(TalatiCumMantri, null, "\u0AA4\u0AB2\u0ABE\u0A9F\u0AC0 \u0A95\u0AAE \u0AAE\u0A82\u0AA4\u0ACD\u0AB0\u0AC0")))));
}
export default WidowCertificateGuj;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14;
//# sourceMappingURL=WidowCertificateGuj.js.map
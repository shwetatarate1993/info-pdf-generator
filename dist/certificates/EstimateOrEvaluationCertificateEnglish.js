import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    margin: 20px;\n  "], ["\n    text-align: center;\n    margin: 20px;\n  "])));
var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n  border: 1px solid black;\n  height: 800px;\n  width: 800px;\n  margin: 0 ;\n"], ["\n  border: 1px solid black;\n  height: 800px;\n  width: 800px;\n  margin: 0 ;\n"])));
var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\n  border: 6px solid black;\n  margin: 0 auto;\n  height: 793px;\n  width: 792px;\n  position: relative;\n  top: 1.7px;\n  left: 0.3px;\n"], ["\n  border: 6px solid black;\n  margin: 0 auto;\n  height: 793px;\n  width: 792px;\n  position: relative;\n  top: 1.7px;\n  left: 0.3px;\n"])));
var EstimateCertificate = styled.div(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n  margin: 0 auto;\n  padding: 30px;\n  text-align: center;\n  position: relative;\n  top: 30px;\n  border: 4px solid black;\n  width: 450px;\n  font-weight: bold;\n  border-radius: 15px;\n  font-size: 30px;\n"], ["\n  margin: 0 auto;\n  padding: 30px;\n  text-align: center;\n  position: relative;\n  top: 30px;\n  border: 4px solid black;\n  width: 450px;\n  font-weight: bold;\n  border-radius: 15px;\n  font-size: 30px;\n"])));
var ParaOne = styled.p(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 20px;\n    left: 100px;\n    font-size: 20px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width : 550px;\n  "], ["\n    position: relative;\n    top: 20px;\n    left: 100px;\n    font-size: 20px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width : 550px;\n  "])));
var GramPanchayat1 = styled.input(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid white;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid white;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
var ParaTwo = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 200px;\n  left: 80px;\n  font-size: 20px;\n"], ["\n  position: absolute;\n  top: 200px;\n  left: 80px;\n  font-size: 20px;\n"])));
var GramPanchayat2 = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
var ParaThree = styled.p(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 250px;\n  left: 80px;\n  font-size: 20px;\n"], ["\n  position: absolute;\n  top: 250px;\n  left: 80px;\n  font-size: 20px;\n"])));
var DateInput = styled.input(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\nfont-size: 20px;\nfont-weight: bold;\nwidth: 150px;\ntext-align: center;\nborder-bottom: 1.5px solid black;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\n"], ["\nfont-size: 20px;\nfont-weight: bold;\nwidth: 150px;\ntext-align: center;\nborder-bottom: 1.5px solid black;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\n"])));
var Taluka = styled.input(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\ntext-align: center;\nfont-size: 20px;\nfont-weight: bold;\nwidth: 290px;\nborder-bottom: 1.5px solid black;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\n"], ["\ntext-align: center;\nfont-size: 20px;\nfont-weight: bold;\nwidth: 290px;\nborder-bottom: 1.5px solid black;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\n"])));
var ParaFour = styled.p(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 300px;\n  left: 80px;\n  font-size: 20px;\n"], ["\n  position: absolute;\n  top: 300px;\n  left: 80px;\n  font-size: 20px;\n"])));
var Name = styled.input(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
var ParaFive = styled.p(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 350px;\n  left: 80px;\n  font-size: 20px;\n"], ["\n  position: absolute;\n  top: 350px;\n  left: 80px;\n  font-size: 20px;\n"])));
var TypeOfBusiness = styled.input(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
var ParaSix = styled.p(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 400px;\n  left: 80px;\n  font-size: 20px;\n  word-spacing: 16px;\n"], ["\n  position: absolute;\n  top: 400px;\n  left: 80px;\n  font-size: 20px;\n  word-spacing: 16px;\n"])));
var ParaSeven = styled.p(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 450px;\n  left: 80px;\n  font-size: 20px;\n"], ["\n  position: absolute;\n  top: 450px;\n  left: 80px;\n  font-size: 20px;\n"])));
var Amount = styled.input(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
var AmountInWords = styled.input(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"], ["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n"])));
var ParaEight = styled.p(templateObject_20 || (templateObject_20 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 500px;\n  left: 80px;\n  font-size: 20px;\n"], ["\n  position: absolute;\n  top: 500px;\n  left: 80px;\n  font-size: 20px;\n"])));
var DateOne = styled.p(templateObject_21 || (templateObject_21 = tslib_1.__makeTemplateObject(["\nposition: absolute;\ntop: 550px;\nleft: 80px;\nfont-size: 20px;\nfont-weight: bold;\nword-spacing: 8px;\n"], ["\nposition: absolute;\ntop: 550px;\nleft: 80px;\nfont-size: 20px;\nfont-weight: bold;\nword-spacing: 8px;\n"])));
var DateInput1 = styled.input(templateObject_22 || (templateObject_22 = tslib_1.__makeTemplateObject(["\nfont-size: 20px;\nfont-weight: bold;\nwidth: 150px;\nborder: none;\n"], ["\nfont-size: 20px;\nfont-weight: bold;\nwidth: 150px;\nborder: none;\n"])));
var ParaNine = styled.p(templateObject_23 || (templateObject_23 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 590px;\n  left: 80px;\n  font-size: 20px;\n  font-weight: bold;\n"], ["\n  position: absolute;\n  top: 590px;\n  left: 80px;\n  font-size: 20px;\n  font-weight: bold;\n"])));
var ParaTen = styled.p(templateObject_24 || (templateObject_24 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 630px;\n  left: 80px;\n  font-size: 20px;\n  font-weight: bold;\n"], ["\n  position: absolute;\n  top: 630px;\n  left: 80px;\n  font-size: 20px;\n  font-weight: bold;\n"])));
var ParaEleven = styled.p(templateObject_25 || (templateObject_25 = tslib_1.__makeTemplateObject(["\n  position: absolute;\n  top: 670px;\n  left: 80px;\n  font-size: 20px;\n"], ["\n  position: absolute;\n  top: 670px;\n  left: 80px;\n  font-size: 20px;\n"])));
var GramPanchayat3 = styled.input(templateObject_26 || (templateObject_26 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
function EstimateOrEvaluationCertificateEnglish(data) {
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var NAME = data.NAME;
    var TYPE_OF_BUSINESS = data.TYPE_OF_BUSINESS;
    var AMOUNT = data.AMOUNT;
    var AMOUNT_IN_WORD = data.AMOUNT_IN_WORD;
    var dateString = data.DATE;
    var dateObj = new Date(dateString);
    var momentObj = moment(dateObj);
    var DATE = momentObj.format('YYYY-MMM-DD');
    return (React.createElement(App, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(ParaOne, null,
                    React.createElement(GramPanchayat1, { value: GRAMPANCHAYAT_NAME }),
                    "Gram Panchayat"),
                React.createElement(EstimateCertificate, null, "Panchkayas"),
                React.createElement(ParaTwo, null,
                    "We have five signatures below. Village:-",
                    React.createElement(GramPanchayat2, { value: GRAMPANCHAYAT_NAME })),
                React.createElement(ParaThree, null,
                    "today the ",
                    React.createElement(DateInput, { value: DATE }),
                    " of Ta:-",
                    React.createElement(Taluka, { value: TALUKA_NAME }),
                    " 's "),
                React.createElement(ParaFour, null,
                    "presence is present on that day, that Shri ",
                    React.createElement(Name, { value: NAME })),
                React.createElement(ParaFive, null,
                    "is the resident of our village and they do ",
                    React.createElement(TypeOfBusiness, { value: TYPE_OF_BUSINESS })),
                React.createElement(ParaSix, null, "in  the  village  and  whose  annual  income  does  not  exceed "),
                React.createElement(ParaSeven, null,
                    "Rs. ",
                    React.createElement(Amount, { value: AMOUNT }),
                    "/- in word ",
                    React.createElement(AmountInWords, { value: AMOUNT_IN_WORD })),
                React.createElement(ParaEight, null, "We are going to write this five year verdict."),
                React.createElement(DateOne, null,
                    "Date: ",
                    React.createElement(DateInput1, { value: DATE }),
                    "  \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0-------------------"),
                React.createElement(ParaNine, null, "In person  \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0-------------------"),
                React.createElement(ParaTen, null, "Talati Cum Mantri Shri  \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0\u00A0 \u00A0 \u00A0-------------------"),
                React.createElement(ParaEleven, null,
                    React.createElement(GramPanchayat3, { value: GRAMPANCHAYAT_NAME }))))));
}
export default EstimateOrEvaluationCertificateEnglish;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20, templateObject_21, templateObject_22, templateObject_23, templateObject_24, templateObject_25, templateObject_26;
//# sourceMappingURL=EstimateOrEvaluationCertificateEnglish.js.map
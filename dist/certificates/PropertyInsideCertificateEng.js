import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  border: 8px solid black;\n  height: 700px;\n  width: 750px;\n  margin: 20px;\n"], ["\n  text-align: center;\n  border: 8px solid black;\n  height: 700px;\n  width: 750px;\n  margin: 20px;\n"])));
var Heading = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n    margin: 0 auto;\n    padding-right: 23px;\n    padding-left: 23px;\n    padding-top:4px;\n    padding-bottom: 15px;\n    text-align: center;\n    position: relative;\n    top: 10px;\n    font-size: 20px;\n    border: 3px solid black;\n    width: 250px;\n    font-weight: bold;\n    border-radius: 11px;\n  "], ["\n    margin: 0 auto;\n    padding-right: 23px;\n    padding-left: 23px;\n    padding-top:4px;\n    padding-bottom: 15px;\n    text-align: center;\n    position: relative;\n    top: 10px;\n    font-size: 20px;\n    border: 3px solid black;\n    width: 250px;\n    font-weight: bold;\n    border-radius: 11px;\n  "])));
var Container = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\n    margin: 0 auto;\n    width: 180px;\n    height: 180px;\n    background: no-repeat;\n    position: relative;\n    top: 30px; \n"], ["\n    margin: 0 auto;\n    width: 180px;\n    height: 180px;\n    background: no-repeat;\n    position: relative;\n    top: 30px; \n"])));
var ParaOne = styled.p(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    top: 50px;\n  "], ["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    top: 50px;\n  "])));
var PropertyName = styled.input(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n    "], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n    "])));
var ParaOneSpan = styled.span(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject([""], [""])));
var ParaTwo = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  top: 40px;\n  "], ["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  top: 40px;\n  "])));
var GrampanchayatName = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 340px;\n    font-family: \"Times New Roman\";\n  "], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 340px;\n    font-family: \"Times New Roman\";\n  "])));
var ParaTwoSpan = styled.span(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject([""], [""])));
var ParaThree = styled.p(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  top: 30px;\n"], ["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  top: 30px;\n"])));
var TalukaName = styled.input(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n  "], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n  "])));
var ParaThreeSpan1 = styled.span(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject([""], [""])));
var DistrictName = styled.input(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n  "], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n  "])));
var ParaThreeSpan2 = styled.span(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject([""], [""])));
var ParaFour = styled.p(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  top: 30px;\n  "], ["\n  position: relative;\n  font-size: 20px;\n  font-weight: normal;\n  top: 30px;\n  "])));
var Name = styled.input(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n  "], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n  "])));
var ParaFourSpan = styled.span(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject([""], [""])));
function PropertyInsideCertificateEng(data) {
    var PROPERTY_NUMBER = data.PROPERTY_NUMBER;
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var DISTRICT_NAME = data.DISTRICT_NAME;
    var NAME = data.NAME;
    var PHOTO = data.PHOTO;
    return (React.createElement(App, { id: 'download-div' },
        React.createElement(Heading, null, "Property Inside Certificate"),
        React.createElement(Container, null,
            React.createElement("img", { src: PHOTO, width: '180px', height: '180px' })),
        React.createElement(ParaOne, null,
            "This certificate is for Property Number",
            React.createElement(PropertyName, { value: PROPERTY_NUMBER }),
            React.createElement(ParaOneSpan, null, "which is ")),
        React.createElement(ParaTwo, null,
            "located \u00A0 in \u00A0 the \u00A0 Gram \u00A0 Panchayat",
            React.createElement(GrampanchayatName, { value: GRAMPANCHAYAT_NAME }),
            React.createElement(ParaTwoSpan, null, "Ta. ")),
        React.createElement(ParaThree, null,
            React.createElement(TalukaName, { value: TALUKA_NAME }),
            React.createElement(ParaThreeSpan1, null, "Dist. "),
            React.createElement(DistrictName, { value: DISTRICT_NAME }),
            React.createElement(ParaThreeSpan2, null, ". This property ")),
        React.createElement(ParaFour, null,
            "is in the Name of Mr.",
            React.createElement(Name, { value: NAME }),
            React.createElement(ParaFourSpan, null, "."))));
}
export default PropertyInsideCertificateEng;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17;
//# sourceMappingURL=PropertyInsideCertificateEng.js.map
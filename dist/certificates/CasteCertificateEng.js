import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
function CasteCertificateEng(data) {
    var TALUKA_NAME = data.TALUKA_NAME;
    var DISTRICT_NAME = data.DISTRICT_NAME;
    var CAST_CATEGORY = data.CAST_CATEGORY;
    var FATHER_NAME = data.FATHER_NAME;
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var NAME = data.NAME;
    var PLACE = data.PLACE;
    var RELIGION = data.RELIGION;
    var PURPOSE = data.PURPOSE;
    var PHOTO = data.PHOTO;
    var dateString = data.DATE;
    var dateObj = new Date(dateString);
    var momentObj = moment(dateObj);
    var DATE = momentObj.format('YYYY-MMM-DD');
    var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n text-align: center;\n margin: 20px;\n"], ["\n text-align: center;\n margin: 20px;\n"])));
    var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\nborder: 1px solid black;\nheight: 800px;\nwidth: 800px;\nmargin: 0 ;\n"], ["\nborder: 1px solid black;\nheight: 800px;\nwidth: 800px;\nmargin: 0 ;\n"])));
    var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\nborder: 6px solid black;\nmargin: 0 auto;\nheight: 793px;\nwidth: 793px;\nposition: relative;\ntop: 1.5px;\nleft: 0.3px;\n"], ["\nborder: 6px solid black;\nmargin: 0 auto;\nheight: 793px;\nwidth: 793px;\nposition: relative;\ntop: 1.5px;\nleft: 0.3px;\n"])));
    var Header = styled.p(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\nmargin: 0 auto;\npadding: 10px;\ntext-align: center;\nposition: relative;\ntop: 20px;\nborder: 4px solid black;\nwidth: 350px;\nfont-weight: bold;\nborder-radius: 15px;\nfont-size: 30px;\n"], ["\nmargin: 0 auto;\npadding: 10px;\ntext-align: center;\nposition: relative;\ntop: 20px;\nborder: 4px solid black;\nwidth: 350px;\nfont-weight: bold;\nborder-radius: 15px;\nfont-size: 30px;\n"])));
    var Photo = styled.div(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n margin: 0 auto;\n position: relative;\n top: 30px;\n width:180px;\n height:180px;\n"], ["\n margin: 0 auto;\n position: relative;\n top: 30px;\n width:180px;\n height:180px;\n"])));
    var Paras = styled.div(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\nposition:relative;\ntop:35px;\n"], ["\nposition:relative;\ntop:35px;\n"])));
    var ParaOne = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n   position: relative;\n   top:20px;\n   font-size: 20px;\n   font-weight: normal;\n   \n "], ["\n   position: relative;\n   top:20px;\n   font-size: 20px;\n   font-weight: normal;\n   \n "])));
    var Name = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:150px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:150px;\n"])));
    var GrampanchayatName = styled.input(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width: 180px;\n \n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width: 180px;\n \n"])));
    var ParaTwo = styled.p(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n   position: relative;\n   top:4px;\n   font-size: 20px;\n   font-weight: normal;\n   \n "], ["\n   position: relative;\n   top:4px;\n   font-size: 20px;\n   font-weight: normal;\n   \n "])));
    var FatherName = styled.input(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 0px solid white;\n text-align: center;\n width:135px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 0px solid white;\n text-align: center;\n width:135px;\n"])));
    var ParaThree = styled.p(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject(["\n   position: relative;\n   font-size: 20px;\n   font-weight: normal;\n   \n   bottom:10px;\n "], ["\n   position: relative;\n   font-size: 20px;\n   font-weight: normal;\n   \n   bottom:10px;\n "])));
    var ParaFour = styled.p(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n   position: relative;\n   bottom:26px;\n   font-size: 20px;\n   font-weight: normal;\n   \n "], ["\n   position: relative;\n   bottom:26px;\n   font-size: 20px;\n   font-weight: normal;\n   \n "])));
    var ParaFive = styled.p(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject(["\n   position: relative;\n   bottom:43px;\n   font-size: 20px;\n   font-weight: normal;\n   right:20px;\n "], ["\n   position: relative;\n   bottom:43px;\n   font-size: 20px;\n   font-weight: normal;\n   right:20px;\n "])));
    var Religion = styled.input(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:95px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:95px;\n"])));
    var Purpose = styled.input(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:130px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:130px;\n"])));
    var CasteCatagary = styled.input(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:170px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:170px;\n"])));
    var TalukaName = styled.input(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:100px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:100px;\n"])));
    var DistrictName = styled.input(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject(["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:90px;\n"], ["\n font-weight: bold;\n font-size: 16px;\n border-bottom: 1.5px solid black;\n border-top: 1px solid white;\n border-right: 1px solid white;\n border-left: 1px solid white;\n text-align: center;\n width:90px;\n"])));
    var PlaceLabel = styled.p(templateObject_20 || (templateObject_20 = tslib_1.__makeTemplateObject(["\nfont-size: 18px;\nfont-weight: normal;\nposition: relative;\nbottom: 5px;\nright:230px;\n"], ["\nfont-size: 18px;\nfont-weight: normal;\nposition: relative;\nbottom: 5px;\nright:230px;\n"])));
    var PlaceLastInput = styled.input(templateObject_21 || (templateObject_21 = tslib_1.__makeTemplateObject(["\nfont-size: 20px;\nfont-weight: bold;\nposition: relative;\nwidth: 100px;\nborder-bottom: 1.5px solid black;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\ntext-align:center;\n\n"], ["\nfont-size: 20px;\nfont-weight: bold;\nposition: relative;\nwidth: 100px;\nborder-bottom: 1.5px solid black;\nborder-top: 1px solid white;\nborder-right: 1px solid white;\nborder-left: 1px solid white;\ntext-align:center;\n\n"])));
    var PlaceInput = styled.input(templateObject_22 || (templateObject_22 = tslib_1.__makeTemplateObject(["\nfont-size: 18px;\nfont-weight: bold;\nposition: relative;\ntext-align : center;\nwidth: 120px;\nborder:none;\n"], ["\nfont-size: 18px;\nfont-weight: bold;\nposition: relative;\ntext-align : center;\nwidth: 120px;\nborder:none;\n"])));
    var DateLabel = styled.p(templateObject_23 || (templateObject_23 = tslib_1.__makeTemplateObject(["\nfont-size: 20px;\nfont-weight: normal;\nposition: relative;\nright: 160px;\nbottom: 5px;\n"], ["\nfont-size: 20px;\nfont-weight: normal;\nposition: relative;\nright: 160px;\nbottom: 5px;\n"])));
    var DateInput = styled.input(templateObject_24 || (templateObject_24 = tslib_1.__makeTemplateObject(["\nfont-size: 18px;\nfont-weight: bold;\nposition: relative;\nborder: none;\n"], ["\nfont-size: 18px;\nfont-weight: bold;\nposition: relative;\nborder: none;\n"])));
    var TalatiCumMantri = styled.label(templateObject_25 || (templateObject_25 = tslib_1.__makeTemplateObject(["\n position: relative;\n font-size: 20px;\n font-weight: normal;\n left:230px;\n bottom:60px;\n \n "], ["\n position: relative;\n font-size: 20px;\n font-weight: normal;\n left:230px;\n bottom:60px;\n \n "])));
    var Pspan1 = styled.span(templateObject_26 || (templateObject_26 = tslib_1.__makeTemplateObject([""], [""])));
    var Pspan2 = styled.span(templateObject_27 || (templateObject_27 = tslib_1.__makeTemplateObject([""], [""])));
    var Pspan3 = styled.span(templateObject_28 || (templateObject_28 = tslib_1.__makeTemplateObject([""], [""])));
    var Pspan4 = styled.span(templateObject_29 || (templateObject_29 = tslib_1.__makeTemplateObject([""], [""])));
    var Pspan5 = styled.span(templateObject_30 || (templateObject_30 = tslib_1.__makeTemplateObject([""], [""])));
    var Pspan6 = styled.span(templateObject_31 || (templateObject_31 = tslib_1.__makeTemplateObject([""], [""])));
    var Pspan7 = styled.span(templateObject_32 || (templateObject_32 = tslib_1.__makeTemplateObject([""], [""])));
    var Pspan8 = styled.span(templateObject_33 || (templateObject_33 = tslib_1.__makeTemplateObject([""], [""])));
    var Pspan9 = styled.span(templateObject_34 || (templateObject_34 = tslib_1.__makeTemplateObject([""], [""])));
    var Pspan10 = styled.span(templateObject_35 || (templateObject_35 = tslib_1.__makeTemplateObject([""], [""])));
    var Pspan11 = styled.span(templateObject_36 || (templateObject_36 = tslib_1.__makeTemplateObject([""], [""])));
    var Pspan12 = styled.span(templateObject_37 || (templateObject_37 = tslib_1.__makeTemplateObject([""], [""])));
    var Footers = styled.div(templateObject_38 || (templateObject_38 = tslib_1.__makeTemplateObject(["\nposition:relative;\ntop:45px;\n"], ["\nposition:relative;\ntop:45px;\n"])));
    return (React.createElement(App, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(Header, null, "Caste Certificate"),
                React.createElement(Photo, null,
                    React.createElement("img", { src: PHOTO, width: '180px', height: '180px' })),
                React.createElement(Paras, null,
                    React.createElement(ParaOne, null,
                        React.createElement(Pspan1, null, "It is said that the father/guardian of Mr."),
                        React.createElement(Name, { value: NAME }),
                        React.createElement(Pspan2, null, "remains Mr.")),
                    React.createElement(ParaTwo, null,
                        React.createElement(FatherName, { value: FATHER_NAME }),
                        React.createElement(Pspan3, null, ".They are the resident of Moj "),
                        React.createElement(GrampanchayatName, { value: GRAMPANCHAYAT_NAME }),
                        React.createElement(Pspan4, null, "Ta.\u00A0 \u00A0 ")),
                    React.createElement(ParaThree, null,
                        React.createElement(TalukaName, { value: TALUKA_NAME }),
                        React.createElement(Pspan5, null, " Dist."),
                        React.createElement(DistrictName, { value: DISTRICT_NAME }),
                        React.createElement(Pspan6, null, ", "),
                        React.createElement(PlaceInput, { value: PLACE }),
                        React.createElement(Pspan7, null, "Fariya/Society.They are from ")),
                    React.createElement(ParaFour, null,
                        React.createElement(Religion, { value: RELIGION }),
                        React.createElement(Pspan8, null, "castes.They include "),
                        React.createElement(CasteCatagary, { value: CAST_CATEGORY }),
                        React.createElement(Pspan9, null, "species.The precise ")),
                    React.createElement(ParaFive, null,
                        React.createElement(Pspan10, null, "instance is given for "),
                        React.createElement(Purpose, { value: PURPOSE }),
                        React.createElement(Pspan11, null, "purposes under the scheme.\u00A0 \u00A0"))),
                React.createElement(Footers, null,
                    React.createElement(PlaceLabel, null,
                        "Place:",
                        React.createElement(PlaceLastInput, { value: PLACE })),
                    React.createElement(DateLabel, null,
                        "Date:",
                        React.createElement(DateInput, { value: DATE })),
                    React.createElement(TalatiCumMantri, null, "Talati Cum Mantri"))))));
}
export default CasteCertificateEng;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20, templateObject_21, templateObject_22, templateObject_23, templateObject_24, templateObject_25, templateObject_26, templateObject_27, templateObject_28, templateObject_29, templateObject_30, templateObject_31, templateObject_32, templateObject_33, templateObject_34, templateObject_35, templateObject_36, templateObject_37, templateObject_38;
//# sourceMappingURL=CasteCertificateEng.js.map
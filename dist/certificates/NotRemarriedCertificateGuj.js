import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
var Default = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    margin: 20px;\n    font-family: \"ChelthmlITC BK BT\";\n    padding:4px;\n  "], ["\n    text-align: center;\n    margin: 20px;\n    font-family: \"ChelthmlITC BK BT\";\n    padding:4px;\n  "])));
var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n    border: 1px solid black;\n    height: 500px;\n    width: 700px;\n    margin: 0 ;\n  "], ["\n    border: 1px solid black;\n    height: 500px;\n    width: 700px;\n    margin: 0 ;\n  "])));
var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 494px;\n    width: 694px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "], ["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 494px;\n    width: 694px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "])));
var NotReMarried = styled.div(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n    margin: 10px auto;\n    padding-right: 23px;\n    padding-left: 23px;\n    padding-top:4px;\n    padding-bottom: 15px;\n    text-align: center;\n    position: relative;\n    font-size: 20px;\n    border: 3px solid black;\n    width: 250px;\n    font-weight: bold;\n    border-radius: 11px;\n    top:2px;\n  "], ["\n    margin: 10px auto;\n    padding-right: 23px;\n    padding-left: 23px;\n    padding-top:4px;\n    padding-bottom: 15px;\n    text-align: center;\n    position: relative;\n    font-size: 20px;\n    border: 3px solid black;\n    width: 250px;\n    font-weight: bold;\n    border-radius: 11px;\n    top:2px;\n  "])));
var ParaOne = styled.p(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    top: 30px;\n  "], ["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    top: 30px;\n  "])));
var Name = styled.input(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top:1.5px solid white;\n    border-right:1.5px solid white;\n    border-left:1.5px solid white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n    \n  "], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top:1.5px solid white;\n    border-right:1.5px solid white;\n    border-left:1.5px solid white;\n    width: 250px;\n    font-family: \"Times New Roman\";\n    \n  "])));
var ParaOneSpan = styled.span(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject([""], [""])));
var ParaTwo = styled.p(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  right:1px;\n  font-size: 20px;\n  font-weight: normal;\n  top: 25px;\n  "], ["\n  position: relative;\n  right:1px;\n  font-size: 20px;\n  font-weight: normal;\n  top: 25px;\n  "])));
var GrampanchayatNamePara = styled.input(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top:1.5px solid white;\n    border-right:1.5px solid white;\n    border-left:1.5px solid white;\n    width: 290px;\n    \n    font-family: \"Times New Roman\";\n  "], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top:1.5px solid white;\n    border-right:1.5px solid white;\n    border-left:1.5px solid white;\n    width: 290px;\n    \n    font-family: \"Times New Roman\";\n  "])));
var ParaTwoSpan = styled.span(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject([""], [""])));
var TalukaInput = styled.input(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n    font-size: 18px;\n    font-weight: bold;\n    text-align: center;\n    position: relative;\n    width: 190px;\n    border-bottom: 1.5px solid black;\n    border-top:1.5px solid white;\n    border-right:1.5px solid white;\n    border-left:1.5px solid white;\n    font-family: \"Times New Roman\";\n  "], ["\n    font-size: 18px;\n    font-weight: bold;\n    text-align: center;\n    position: relative;\n    width: 190px;\n    border-bottom: 1.5px solid black;\n    border-top:1.5px solid white;\n    border-right:1.5px solid white;\n    border-left:1.5px solid white;\n    font-family: \"Times New Roman\";\n  "])));
var ParaTwoSpan2 = styled.span(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject([""], [""])));
var ParaThree = styled.p(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    top:25px;\n    \n  "], ["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    top:25px;\n    \n  "])));
var DistrictName = styled.input(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject(["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative; \n    border-bottom: 1.5px solid black;\n    border-top:1.5px solid white;\n    border-right:1.5px solid white;\n    border-left:1.5px solid white;\n    text-align: center;\n    font-family: \"Times New Roman\";\n  "], ["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative; \n    border-bottom: 1.5px solid black;\n    border-top:1.5px solid white;\n    border-right:1.5px solid white;\n    border-left:1.5px solid white;\n    text-align: center;\n    font-family: \"Times New Roman\";\n  "])));
var Para3Span1 = styled.span(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject([""], [""])));
var DateOne = styled.input(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject(["\n  font-size: 18px;\n  font-weight: bold;\n  text-align: center;\n  position: relative;\n  border-bottom: 1.5px solid black;\n  border-top:1.5px solid white;\n  border-right:1.5px solid white;\n  border-left:1.5px solid white;\n  font-family: \"Times New Roman\";\n  "], ["\n  font-size: 18px;\n  font-weight: bold;\n  text-align: center;\n  position: relative;\n  border-bottom: 1.5px solid black;\n  border-top:1.5px solid white;\n  border-right:1.5px solid white;\n  border-left:1.5px solid white;\n  font-family: \"Times New Roman\";\n  "])));
var Para3Span2 = styled.span(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject([""], [""])));
var ParaFour = styled.p(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    right: 6px;\n    top: 25px;\n  "], ["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    right: 6px;\n    top: 25px;\n  "])));
var ParaFive = styled.p(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    top: 30px;\n    right: 255px;\n  "], ["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    top: 30px;\n    right: 255px;\n  "])));
var SarpanchName = styled.label(templateObject_20 || (templateObject_20 = tslib_1.__makeTemplateObject(["\n  font-size: 20px;\n  position: relative;\n  font-weight: normal;\n  right:180px;\n  top:100px;\n  "], ["\n  font-size: 20px;\n  position: relative;\n  font-weight: normal;\n  right:180px;\n  top:100px;\n  "])));
var TalatiCumMantriShri = styled.label(templateObject_21 || (templateObject_21 = tslib_1.__makeTemplateObject(["\n  font-size: 20px;\n  position: relative;\n  font-weight: normal;\n  left:165px;\n  top:100px;\n  "], ["\n  font-size: 20px;\n  position: relative;\n  font-weight: normal;\n  left:165px;\n  top:100px;\n  "])));
function NotRemarriedCertificateGuj(data) {
    var TALUKA_NAME = data.TALUKA_NAME;
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var DISTRICT_NAME = data.DISTRICT_NAME;
    var NAME = data.NAME;
    var dateString = data.DATE;
    var dateObj = new Date(dateString);
    var momentObj = moment(dateObj);
    var DATE = momentObj.format('YYYY-MMM-DD');
    return (React.createElement(Default, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(NotReMarried, null, "\u0AAA\u0AC1\u0AA8\u0A83\u0AB2\u0A97\u0ACD\u0AA8 \u0A95\u0AB0\u0AC7\u0AB2 \u0AA8\u0AA5\u0AC0 \u0AA6\u0A96\u0AB2\u0ACB"),
                React.createElement(ParaOne, null,
                    "\u0A86\u0AA5\u0AC0 \u0AA6\u0ABE\u0A96\u0AB2\u0ACB \u0A86\u0AAA\u0AB5\u0ABE\u0AAE\u0ABE \u0A86\u0AB5\u0AC7 \u0A9B\u0AC7 \u0A95\u0AC7 \u0AAE\u0ACB\u0A9C\u0AC7 :",
                    React.createElement(Name, { value: GRAMPANCHAYAT_NAME }),
                    React.createElement(ParaOneSpan, null, "\u00A0 \u0AAE\u0ABE\u0A82\u0AA5\u0AC0 ")),
                React.createElement(ParaTwo, null,
                    "\u0AA4\u0ABE.\u00A0",
                    React.createElement(GrampanchayatNamePara, { value: TALUKA_NAME }),
                    React.createElement(ParaTwoSpan, null, "\u0A9C\u0ABF."),
                    "\u00A0\u00A0\u00A0\u00A0",
                    React.createElement(TalukaInput, { value: DISTRICT_NAME }),
                    "\u00A0\u00A0",
                    React.createElement(ParaTwoSpan2, null, "\u0AA8\u0ABE \u0AB0\u0AB9\u0AC0\u0AB6 \u0A9C\u0AC7  .")),
                React.createElement(ParaThree, null,
                    React.createElement(DistrictName, { value: NAME }),
                    React.createElement(Para3Span1, null, "\u0A9C\u0AC7\u0A93\u0AA8\u0ABE \u0AAA\u0AA4\u0ABF \u0AA4\u0ABE : "),
                    React.createElement(DateOne, { value: DATE }),
                    React.createElement(Para3Span2, null, "\u0AA8\u0ABE \u0AB0\u0ACB\u0A9C  ")),
                React.createElement(ParaFour, null, "\u0AAE\u0AB0\u0AA3 \u0AAA\u0ABE\u0AAE\u0AC7\u0AB2 \u0A9B\u0AC7. \u0AA4\u0ACD\u0AAF\u0ABE\u0AB0 \u0AAC\u0ABE\u0AA6 \u0AA4\u0AC7\u0AAE\u0AA3\u0AC7 \u0AAA\u0AC1\u0AA8\u0A83\u0AB2\u0A97\u0ACD\u0AA8 \u0A95\u0AB0\u0AC7\u0AB2 \u0AA8\u0AA5\u0AC0.\u0A9C\u0AC7 \u0A96\u0ABE\u0AA4\u0ACD\u0AB0\u0AC0 \u0A95\u0AB0\u0AC0 \u0A86 \u0AA6\u0ABE\u0A96\u0AB2\u0ACB "),
                React.createElement(ParaFive, null, "\u0A86\u0AAA\u0AB5\u0ABE\u0AAE\u0ABE \u0A86\u0AB5\u0AC7 \u0A9B\u0AC7."),
                React.createElement(SarpanchName, null, "\u0AB8\u0AB0\u0AAA\u0A82\u0A9A \u0AB6\u0ACD\u0AB0\u0AC0"),
                React.createElement(TalatiCumMantriShri, null, "\u0AA4\u0AB2\u0ABE\u0A9F\u0AC0 \u0A95\u0AAE \u0AAE\u0A82\u0AA4\u0ACD\u0AB0\u0AC0 \u0AB6\u0ACD\u0AB0\u0AC0")))));
}
export default NotRemarriedCertificateGuj;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20, templateObject_21;
//# sourceMappingURL=NotRemarriedCertificateGuj.js.map
import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
function IncomeCertificateGujarati(data) {
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var NAME = data.NAME;
    var FY_YEAR = data.FY_YEAR;
    var FATHER_OR_MOTHER_NAME = data.FATHER_OR_MOTHER_NAME;
    var AMOUNT = data.AMOUNT;
    var AMOUNT_IN_WORD = data.AMOUNT_IN_WORD;
    var TYPE_OF_BUSINESS = data.TYPE_OF_BUSINESS;
    var PLACE = data.PLACE;
    var PURPOSE = data.PURPOSE;
    var dateString = data.DATE;
    var dateObj = new Date(dateString);
    var momentObj = moment(dateObj);
    var DATE = momentObj.format('YYYY-MMM-DD');
    var App = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    margin: 20px;\n  "], ["\n    text-align: center;\n    margin: 20px;\n  "])));
    var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n    border: 1px solid black;\n    height: 800px;\n    width: 800px;\n    margin: 0;\n  "], ["\n    border: 1px solid black;\n    height: 800px;\n    width: 800px;\n    margin: 0;\n  "])));
    var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 793px;\n    width: 793px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "], ["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 793px;\n    width: 793px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "])));
    var IncomeCertificate = styled.div(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n    margin: 0 auto;\n    padding: 30px;\n    text-align: center;\n    position: relative;\n    top: 50px;\n    border: 4px solid black;\n    width: 300px;\n    font-weight: bold;\n    border-radius: 15px;\n    font-size: 30px;\n  "], ["\n    margin: 0 auto;\n    padding: 30px;\n    text-align: center;\n    position: relative;\n    top: 50px;\n    border: 4px solid black;\n    width: 300px;\n    font-weight: bold;\n    border-radius: 15px;\n    font-size: 30px;\n  "])));
    var ParaOne = styled.p(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 80px;\n    left: 20px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 5px;\n  "], ["\n    position: relative;\n    top: 80px;\n    left: 20px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 5px;\n  "])));
    var Name = styled.input(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaTwo = styled.p(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 70px;\n    right: 8px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 20px;\n  "], ["\n    position: relative;\n    top: 70px;\n    right: 8px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 20px;\n  "])));
    var FatherOrMotherName = styled.input(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 280px;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 280px;\n  "])));
    var ParaTwoOne = styled.span(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject([""], [""])));
    var GramPanchayat = styled.input(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 280px;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 280px;\n  "])));
    var ParaThree = styled.p(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 60px;\n    right: 15px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 5px;\n  "], ["\n    position: relative;\n    top: 60px;\n    right: 15px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 5px;\n  "])));
    var Taluka = styled.input(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 210px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 210px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaThreeOne = styled.span(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject([""], [""])));
    var GramPanchayatOne = styled.input(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 210px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 210px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaFour = styled.p(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 49px;\n    right: 10px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 6px;\n  "], ["\n    position: relative;\n    top: 49px;\n    right: 10px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 6px;\n  "])));
    var TypeOfBusiness = styled.input(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 290px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 290px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaFourOne = styled.span(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject([""], [""])));
    var FinancialYear = styled.input(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 290px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    width: 290px;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
    var ParaFourTwo = styled.span(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject([""], [""])));
    var ParaFive = styled.p(templateObject_20 || (templateObject_20 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  top: 39px;\n  right: 10px;\n  font-size: 20px;\n  font-weight: bold;\n  word-spacing: 5px;\n"], ["\n  position: relative;\n  top: 39px;\n  right: 10px;\n  font-size: 20px;\n  font-weight: bold;\n  word-spacing: 5px;\n"])));
    var Amount = styled.input(templateObject_21 || (templateObject_21 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 180px;\n  "], ["\n    text-align: center;\n    font-size: 20px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 180px;\n  "])));
    var ParaFiveOne = styled.span(templateObject_22 || (templateObject_22 = tslib_1.__makeTemplateObject([""], [""])));
    var AmountInWord = styled.input(templateObject_23 || (templateObject_23 = tslib_1.__makeTemplateObject(["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n  width: 180px;\n"], ["\n  text-align: center;\n  font-size: 20px;\n  font-weight: bold;\n  border-bottom: 1.5px solid black;\n  border-top: 1px solid white;\n  border-right: 1px solid white;\n  border-left: 1px solid white;\n  width: 180px;\n"])));
    var ParaSix = styled.p(templateObject_24 || (templateObject_24 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 32px;\n    right: 55px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 5px;\n  "], ["\n    position: relative;\n    top: 32px;\n    right: 55px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 5px;\n  "])));
    var Purpose = styled.p(templateObject_25 || (templateObject_25 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 50px;\n    right: 215px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 8px;\n  "], ["\n    position: relative;\n    top: 50px;\n    right: 215px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 8px;\n  "])));
    var PurposeInput = styled.input(templateObject_26 || (templateObject_26 = tslib_1.__makeTemplateObject(["\n    font-size: 20px;\n    font-weight: bold;\n    width: 150px;\n    border: none;\n  "], ["\n    font-size: 20px;\n    font-weight: bold;\n    width: 150px;\n    border: none;\n  "])));
    var Place = styled.p(templateObject_27 || (templateObject_27 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 50px;\n    right: 215px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 8px;\n  "], ["\n    position: relative;\n    top: 50px;\n    right: 215px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 8px;\n  "])));
    var PlaceInput = styled.input(templateObject_28 || (templateObject_28 = tslib_1.__makeTemplateObject(["\n    font-size: 20px;\n    font-weight: bold;\n    width: 150px;\n    border: none;\n  "], ["\n    font-size: 20px;\n    font-weight: bold;\n    width: 150px;\n    border: none;\n  "])));
    var DateOne = styled.p(templateObject_29 || (templateObject_29 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    top: 50px;\n    right: 210px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 8px;\n  "], ["\n    position: relative;\n    top: 50px;\n    right: 210px;\n    font-size: 20px;\n    font-weight: bold;\n    word-spacing: 8px;\n  "])));
    var DateInput = styled.input(templateObject_30 || (templateObject_30 = tslib_1.__makeTemplateObject(["\n    font-size: 20px;\n    font-weight: bold;\n    width: 150px;\n    border: none;\n  "], ["\n    font-size: 20px;\n    font-weight: bold;\n    width: 150px;\n    border: none;\n  "])));
    return (React.createElement(App, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(IncomeCertificate, null, "\u0A86\u0AB5\u0A95\u0AA8\u0AC1 \u0AAA\u0ACD\u0AB0\u0AAE\u0ABE\u0AA3\u0AAA\u0AA4\u0ACD\u0AB0"),
                React.createElement(ParaOne, null,
                    "\u0A86\u0AA5\u0AC0 \u0AA6\u0ABE\u0A96\u0AB2\u0ACB \u0A86\u0AAA\u0AB5\u0ABE\u0AAE\u0ABE \u0A86\u0AB5\u0AC7 \u0A9B\u0AC7 \u0A95\u0AC7 \u0AB6\u0ACD\u0AB0\u0AC0 - \u00A0",
                    React.createElement(Name, { value: NAME })),
                React.createElement(ParaTwo, null,
                    "\u0AAA\u0ABF\u0AA4\u0ABE\u0AB6\u0ACD\u0AB0\u0AC0",
                    React.createElement(FatherOrMotherName, { value: FATHER_OR_MOTHER_NAME }),
                    React.createElement(ParaTwoOne, null, "\u0A97\u0ABE\u0AAE :-"),
                    React.createElement(GramPanchayat, { value: GRAMPANCHAYAT_NAME }),
                    "."),
                React.createElement(ParaThree, null,
                    "\u0AA4\u0ABE. \u00A0\u00A0",
                    React.createElement(Taluka, { value: TALUKA_NAME }),
                    "\u00A0\u00A0",
                    React.createElement(ParaThreeOne, null, " \u0AA8\u0ABE \u0AB0\u0AB9\u0AC0\u0AB6 \u0A9B\u0AC7 \u0A9C\u0A93\u0AC7 -"),
                    "\u00A0\u00A0",
                    React.createElement(GramPanchayatOne, { value: GRAMPANCHAYAT_NAME })),
                React.createElement(ParaFour, null,
                    "\u00A0",
                    React.createElement(TypeOfBusiness, { value: TYPE_OF_BUSINESS }),
                    "\u00A0",
                    React.createElement(ParaFourOne, null, "in"),
                    "\u00A0",
                    React.createElement(FinancialYear, { value: FY_YEAR }),
                    "\u00A0",
                    React.createElement(ParaFourTwo, null, "\u0AA8\u0AC0 \u0AB5\u0ABE\u0AB0\u0ACD\u0AB7\u0ABF\u0A95 ")),
                React.createElement(ParaFive, null,
                    "\u0A86\u0AB5\u0A95 \u0AB0\u0AC1\u0AAA\u0ABF\u0AAF\u0ABE \u0AB0\u0AC1 - \u00A0",
                    React.createElement(Amount, { value: AMOUNT }),
                    "\u00A0",
                    React.createElement(ParaFiveOne, null, "/- \u0A85\u0A82\u0A95\u0AC7 \u0AB0\u0AC1\u0AAA\u0ABF\u0AAF\u0ABE"),
                    "\u00A0\u00A0",
                    React.createElement(AmountInWord, { value: AMOUNT_IN_WORD })),
                React.createElement(ParaSix, null, "\u0A95\u0AB0\u0AA4\u0ABE \u0AB5\u0AA7\u0AC1 \u0AA5\u0AA4\u0AC0 \u0AA8\u0AA5\u0AC0 \u0A9C\u0AC7\u0AA8\u0AC0 \u0A96\u0ABE\u0AA4\u0ACD\u0AB0\u0AC0 \u0A95\u0AB0\u0AC0 \u0A86 \u0AA6\u0ABE\u0A96\u0AB2\u0ACB \u0A86\u0AAA\u0AB5\u0ABE\u0AAE\u0ABE \u0A86\u0AB5\u0AC7 \u0A9B\u0AC7 ."),
                React.createElement(Purpose, null,
                    "\u0AB9\u0AC7\u0AA4\u0AC1 - \u00A0",
                    React.createElement(PurposeInput, { value: PURPOSE })),
                React.createElement(Place, null,
                    "\u0AB8\u0ACD\u0AA5\u0AB3 - \u00A0",
                    React.createElement(PlaceInput, { value: PLACE })),
                React.createElement(DateOne, null,
                    "\u0AA4\u0ABE\u0AB0\u0AC0\u0A96 - \u00A0",
                    React.createElement(DateInput, { value: DATE }))))));
}
export default IncomeCertificateGujarati;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20, templateObject_21, templateObject_22, templateObject_23, templateObject_24, templateObject_25, templateObject_26, templateObject_27, templateObject_28, templateObject_29, templateObject_30;
//# sourceMappingURL=IncomeCertificateGujarati.js.map
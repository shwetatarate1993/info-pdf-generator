import * as tslib_1 from "tslib";
import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
var Default = styled.div(templateObject_1 || (templateObject_1 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    margin: 20px;\n    font-family: \"ChelthmlITC BK BT\";\n  "], ["\n    text-align: center;\n    margin: 20px;\n    font-family: \"ChelthmlITC BK BT\";\n  "])));
var BorderOne = styled.div(templateObject_2 || (templateObject_2 = tslib_1.__makeTemplateObject(["\n    border: 1px solid black;\n    height: 400px;\n    width: 700px;\n    margin: 0 ;\n  "], ["\n    border: 1px solid black;\n    height: 400px;\n    width: 700px;\n    margin: 0 ;\n  "])));
var BorderTwo = styled.div(templateObject_3 || (templateObject_3 = tslib_1.__makeTemplateObject(["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 393px;\n    width: 692px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "], ["\n    border: 6px solid black;\n    margin: 0 auto;\n    height: 393px;\n    width: 692px;\n    position: relative;\n    top: 1.5px;\n    left: 0.3px;\n  "])));
var NotReMarried = styled.div(templateObject_4 || (templateObject_4 = tslib_1.__makeTemplateObject(["\n    margin: 0 auto;\n    padding-right: 23px;\n    padding-left: 23px;\n    padding-top:4px;\n    padding-bottom: 15px;\n    text-align: center;\n    position: relative;\n    font-size: 20px;\n    border: 3px solid black;\n    width: 250px;\n    font-weight: bold;\n    border-radius: 11px;\n    top:2px;\n  "], ["\n    margin: 0 auto;\n    padding-right: 23px;\n    padding-left: 23px;\n    padding-top:4px;\n    padding-bottom: 15px;\n    text-align: center;\n    position: relative;\n    font-size: 20px;\n    border: 3px solid black;\n    width: 250px;\n    font-weight: bold;\n    border-radius: 11px;\n    top:2px;\n  "])));
var ParaOne = styled.p(templateObject_5 || (templateObject_5 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    \n    font-size: 20px;\n    font-weight: normal;\n    top: 30px;\n  "], ["\n    position: relative;\n    \n    font-size: 20px;\n    font-weight: normal;\n    top: 30px;\n  "])));
var Name = styled.input(templateObject_6 || (templateObject_6 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 200px;\n    font-family: \"Times New Roman\";\n    \n  "], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n    width: 200px;\n    font-family: \"Times New Roman\";\n    \n  "])));
var ParaOneSpan = styled.span(templateObject_7 || (templateObject_7 = tslib_1.__makeTemplateObject([""], [""])));
var ParaTwo = styled.p(templateObject_8 || (templateObject_8 = tslib_1.__makeTemplateObject(["\n  position: relative;\n  right:1px;\n  font-size: 20px;\n  font-weight: normal;\n  top: 15px;\n  "], ["\n  position: relative;\n  right:1px;\n  font-size: 20px;\n  font-weight: normal;\n  top: 15px;\n  "])));
var GrampanchayatNamePara = styled.input(templateObject_9 || (templateObject_9 = tslib_1.__makeTemplateObject(["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    width: 290px;\n    border:none;\n    font-family: \"Times New Roman\";\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    text-align: center;\n    font-size: 18px;\n    font-weight: bold;\n    width: 290px;\n    border:none;\n    font-family: \"Times New Roman\";\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
var ParaTwoSpan = styled.span(templateObject_10 || (templateObject_10 = tslib_1.__makeTemplateObject([""], [""])));
var TalukaInput = styled.input(templateObject_11 || (templateObject_11 = tslib_1.__makeTemplateObject(["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative;\n    width: 190px;\n    text-align: center;\n    font-family: \"Times New Roman\";\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative;\n    width: 190px;\n    text-align: center;\n    font-family: \"Times New Roman\";\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
var ParaTwoSpan2 = styled.span(templateObject_12 || (templateObject_12 = tslib_1.__makeTemplateObject([""], [""])));
var ParaThree = styled.p(templateObject_13 || (templateObject_13 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n  "], ["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n  "])));
var DistrictName = styled.input(templateObject_14 || (templateObject_14 = tslib_1.__makeTemplateObject(["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative; \n    width: 188px;\n    text-align: center;\n    font-family: \"Times New Roman\";\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n    font-size: 18px;\n    font-weight: bold;\n    position: relative; \n    width: 188px;\n    text-align: center;\n    font-family: \"Times New Roman\";\n    border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
var Para3Span1 = styled.span(templateObject_15 || (templateObject_15 = tslib_1.__makeTemplateObject([""], [""])));
var DateOne = styled.input(templateObject_16 || (templateObject_16 = tslib_1.__makeTemplateObject(["\n  font-size: 18px;\n  font-weight: bold;\n  text-align: center;\n  position: relative;\n  width: 105px;\n  font-family: \"Times New Roman\";\n  border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "], ["\n  font-size: 18px;\n  font-weight: bold;\n  text-align: center;\n  position: relative;\n  width: 105px;\n  font-family: \"Times New Roman\";\n  border-bottom: 1.5px solid black;\n    border-top: 1px solid white;\n    border-right: 1px solid white;\n    border-left: 1px solid white;\n  "])));
var Para3Span2 = styled.span(templateObject_17 || (templateObject_17 = tslib_1.__makeTemplateObject([""], [""])));
var ParaFour = styled.p(templateObject_18 || (templateObject_18 = tslib_1.__makeTemplateObject(["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    right: 6px;\n    bottom: 15px;\n  "], ["\n    position: relative;\n    font-size: 20px;\n    font-weight: normal;\n    right: 6px;\n    bottom: 15px;\n  "])));
var SarpanchName = styled.label(templateObject_19 || (templateObject_19 = tslib_1.__makeTemplateObject(["\n  font-size: 20px;\n  position: relative;\n  font-weight: normal;\n  right:163px;\n  top:20px;\n  "], ["\n  font-size: 20px;\n  position: relative;\n  font-weight: normal;\n  right:163px;\n  top:20px;\n  "])));
var TalatiCumMantriShri = styled.label(templateObject_20 || (templateObject_20 = tslib_1.__makeTemplateObject(["\n  font-size: 20px;\n  position: relative;\n  font-weight: normal;\n  left:165px;\n  top:20px;\n  "], ["\n  font-size: 20px;\n  position: relative;\n  font-weight: normal;\n  left:165px;\n  top:20px;\n  "])));
function NotRemarriedCertificateEng(data) {
    var NAME = data.NAME;
    var GRAMPANCHAYAT_NAME = data.GRAMPANCHAYAT_NAME;
    var TALUKA_NAME = data.TALUKA_NAME;
    var DISTRICT_NAME = data.DISTRICT_NAME;
    var dateString = data.DATE;
    var dateObj = new Date(dateString);
    var momentObj = moment(dateObj);
    var DATE = momentObj.format('YYYY-MMM-DD');
    return (React.createElement(Default, null,
        React.createElement(BorderOne, { id: 'download-div' },
            React.createElement(BorderTwo, null,
                React.createElement(NotReMarried, null, "Not Remarried Certificate"),
                React.createElement(ParaOne, null,
                    "This \u00A0\u00A0\u00A0 is \u00A0\u00A0\u00A0 the \u00A0\u00A0\u00A0 certificate \u00A0\u00A0\u00A0 is \u00A0\u00A0\u00A0 given \u00A0\u00A0\u00A0 to:",
                    React.createElement(Name, { value: NAME }),
                    React.createElement(ParaOneSpan, null, "\u00A0\u00A0 from ")),
                React.createElement(ParaTwo, null,
                    "village\u00A0",
                    React.createElement(GrampanchayatNamePara, { value: GRAMPANCHAYAT_NAME }),
                    React.createElement(ParaTwoSpan, null, "Ta."),
                    "\u00A0\u00A0\u00A0\u00A0",
                    React.createElement(TalukaInput, { value: TALUKA_NAME }),
                    "\u00A0\u00A0",
                    React.createElement(ParaTwoSpan2, null, "Dist.")),
                React.createElement(ParaThree, null,
                    React.createElement(DistrictName, { value: DISTRICT_NAME }),
                    React.createElement(Para3Span1, null, ", whose husband died on date, "),
                    React.createElement(DateOne, { value: DATE }),
                    React.createElement(Para3Span2, null, ". After that,")),
                React.createElement(ParaFour, null, "they did not relinquish. This is done by ensuring that this certificate is given."),
                React.createElement(SarpanchName, null, "Sarpanch Shri"),
                React.createElement(TalatiCumMantriShri, null, "Talati Cum Mantri Shri")))));
}
export default NotRemarriedCertificateEng;
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15, templateObject_16, templateObject_17, templateObject_18, templateObject_19, templateObject_20;
//# sourceMappingURL=NotRemarriedCertificateEng.js.map